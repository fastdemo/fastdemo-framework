/**
 * 
 */
package com.fastdeme.test;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @date 2018年9月4日
 * @author guxingchun
 */
public class LogTest {
	
	private static final Logger logger = LoggerFactory.getLogger(LogTest.class);

	@Test
	@Ignore
	public void test() {
		logger.debug("this is `debug` logger info, isDebugEnabled:" + logger.isDebugEnabled());
		logger.info("this is `info` logger info, isInfoEnabled:" + logger.isInfoEnabled());
		logger.warn("this is `warn` logger info, isWarnEnabled:" + logger.isWarnEnabled());
		logger.error("this is `error` logger info, isErrorEnabled:" + logger.isErrorEnabled());
	}

}
