package com.fastdemo.framework.entity;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Search<T extends BaseEntity> {
	
	public static final String SEARCH_NAME = "search";

	private T queryParams;

	private Page page;
	
	private List<Order> orders;
	
	private List<T> entities;
	
	private List<Property> properties;
	
	public void setOrder(String propertyName,String dir){
		if(orders == null){
			orders = new ArrayList<Order>();
		}
		Order order = new Order();
		order.setPropertyName(propertyName);
		order.setDir(dir);
		orders.add(order);
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public T getQueryParams() {
		return queryParams;
	}

	public void setQueryParams(T queryParams) {
		this.queryParams = queryParams;
	}

	public List<T> getEntities() {
		return entities;
	}

	public void setEntities(List<T> entities) {
		this.entities = entities;
	}
	
	public boolean hasNextPage(){
		return this.getPage().hasNext();
	}
	
	public Search<T> nextSearch(){
		Search<T> nextSearch = new Search<T>();
		nextSearch.setQueryParams(this.queryParams);
		nextSearch.setOrders(this.orders);
		nextSearch.setPage(this.page.nextPage());
		nextSearch.setProperties(this.properties);
		return nextSearch;
	}

	public List<Property> getProperties() {
		return properties;
	}

	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

	public void addProperties(Property property) {
		if(this.properties == null){
			this.properties = new ArrayList<Property>();
		}
		properties.add(property);
	}
	
}
