package com.fastdemo.framework.entity;

import java.util.List;

public class Property {

	private List<?> propertyValues;
	
	private String propertyName;
	
	public List<?> getPropertyValues() {
		return propertyValues;
	}

	public void setPropertyValues(List<?> propertyValues) {
		this.propertyValues = propertyValues;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

}
