package com.fastdemo.framework.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Order {

	public static final String ORDER_NAME = "order";
	
	public static final String ORDER_ASC = "ASC";
	
	public static final String ORDER_DESC = "DESC";
	
	private String propertyName;

	private String dir;

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

}
