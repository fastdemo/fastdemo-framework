package com.fastdemo.framework.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fastdemo.framework.constant.DefaultConstants;

@JsonInclude(Include.NON_NULL)
public class Page {
	
	public static final String PAGE_NAME = "page";
	
	/**
	 * 一页个数
	 */
	private Integer pageSize = DefaultConstants.PAGE_SIZE;
	
	/**
	 * 总记录数
	 */
	private Integer totalCount;
	
	/**
	 * 当前页
	 */
	private Integer currentPage = 1;
	
	/**
	 * 当前个数
	 */
	private Integer currentSize;

	/**
	 * 页数
	 */
	private Integer pageCount;

	/**
	 * 开始记录,从1开始
	 */
	private Integer start = 1;
	
	/**
	 * 结束记录
	 */
	private Integer end;
	
	/**
	 * 指定当前返回个数
	 */
	private Integer customSize;
	
	public Integer getCustomSize() {
		return customSize;
	}

	public void setCustomSize(Integer customSize) {
		this.customSize = customSize;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
		pageCount = (totalCount + pageSize - 1) / pageSize;
		resetPisition();
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
		resetPisition();
	}

	public Integer getCurrentSize() {
		return currentSize;
	}

	public void setCurrentSize(Integer currentSize) {
		this.currentSize = currentSize;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getEnd() {
		return end;
	}

	public void setEnd(Integer end) {
		this.end = end;
	}
	public boolean hasPrevious() {
		if (this.currentPage > 1) {
			return true;
		}
		return false;
	}

	public boolean hasNext() {
		if (this.currentPage < this.pageCount) {
			return true;
		}
		return false;
	}
	
	public Page nextPage() {
		Page newPage = new Page();
		newPage.setCurrentPage(currentPage + 1);
		newPage.setPageSize(pageSize);
		newPage.setStart(pageSize * (newPage.getCurrentPage() -1) + 1);
		return newPage;
	}

	public Page previousPage() {
		Page newPage = new Page();
		newPage.setCurrentPage(currentPage - 1);
		newPage.setPageSize(pageSize);
		return newPage;
	}
	
	private void resetPisition() {
		this.start = this.pageSize * (this.currentPage - 1) + 1;
		this.end = this.currentPage * this.pageSize;
		if (totalCount != null && this.end > totalCount) {
			this.end = totalCount;
		}
		this.currentSize = this.end - this.start + 1;
	}
	
}
