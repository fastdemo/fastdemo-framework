package com.fastdemo.framework.service;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.fastdemo.framework.entity.BaseEntity;
import com.fastdemo.framework.mapper.BaseMapper;
import com.fastdemo.framework.utils.DateUtils;
import com.fastdemo.framework.utils.JsonUtils;
import com.fastdemo.framework.utils.ThreadDataUtils;


/**
 * 服务层基类<br>
 * <br>
 * 封装基础操作,增删改查等,具体实现类中可以通过覆盖方式扩展具体实现方法
 *
 * @param <T>
 */
public abstract class AbstractEntityService<T extends BaseEntity> implements EntityService<T> {

	protected Class<T> clazz;

	@SuppressWarnings("unchecked")
	public AbstractEntityService() {

		Type genericSuperClass = getClass().getGenericSuperclass();

		ParameterizedType parametrizedType = null;
		while (parametrizedType == null) {
			if ((genericSuperClass instanceof ParameterizedType)) {
				parametrizedType = (ParameterizedType) genericSuperClass;
			} else {
				genericSuperClass = ((Class<?>) genericSuperClass).getGenericSuperclass();
			}
		}
		clazz = (Class<T>) parametrizedType.getActualTypeArguments()[0];
	}

	public Class<T> getEntityType() {
		return clazz;
	}

	public String getEntitySimpleName() {
		return clazz.getSimpleName();
	}

	/**
	 * AbstractEntityService log
	 */
	private static final Logger logger = LoggerFactory.getLogger(AbstractEntityService.class);

	/**
	 * sub class must implements this method
	 * 
	 * @return
	 */
	public abstract BaseMapper<T> getMapper();

	/**
	 * 新增
	 * 
	 */
	@Override
	@Transactional
	public int insert(T entity) {
		logger.debug("insert " + clazz.getSimpleName() + ":" + entityToSerialString(entity));
		if (entity != null) {
			if (entity.getCreateTime() == null)
				entity.setCreateTime(DateUtils.getTimestamp());
			if (entity.getCreateUser() == null)
				entity.setCreateUser(getUserIdFromThreadData());
			if (entity.getCreateUserName() == null)
				entity.setCreateUserName(getUserRealNameFromThreadData());			
			if (entity.getUpdateTime() == null)
				entity.setUpdateTime(DateUtils.getTimestamp());
			if (entity.getUpdateUser() == null)
				entity.setUpdateUser(getUserIdFromThreadData());
			if (entity.getUpdateUserName() == null)
				entity.setUpdateUserName(getUserRealNameFromThreadData());
		}
		return getMapper().insert(entity);
	}

	/**
	 * 新增或更新
	 */
	@Override
	@Transactional
	public int insertOrUpdate(T entity) {
		logger.debug("insertOrUpdate " + clazz.getSimpleName() + ":" + entityToSerialString(entity));
		String id = entity.getId();
		String date = DateUtils.getTimestamp();
		if (StringUtils.isBlank(id)) {
			if (entity != null) {
				if (entity.getCreateTime() == null)
					entity.setCreateTime(DateUtils.getTimestamp());
				if (entity.getCreateUser() == null)
					entity.setCreateUser(getUserIdFromThreadData());
				if (entity.getCreateUserName() == null)
					entity.setCreateUserName(getUserRealNameFromThreadData());
			}
			return this.insert(entity);
		} else {
			if (entity != null) {
				if (entity.getUpdateTime() == null)
					entity.setUpdateTime(date);
				if (entity.getUpdateUser() == null)
					entity.setUpdateUser(getUserIdFromThreadData());
				if (entity.getUpdateUserName() == null)
					entity.setUpdateUserName(getUserRealNameFromThreadData());
			}
			return this.updateById(entity);
		}
	}

	/**
	 * delete by id 物理删除
	 */
	@Override
	@Transactional
	public int deleteById(String id) {
		logger.debug("realDeleteById " + clazz.getSimpleName() + ":" + id);
		return getMapper().deleteById(id);
	}

	/**
	 * 条件删除 物理删除
	 * 
	 */
	@Override
	@Transactional
	public int deleteBySelective(T entity) {
		logger.debug("deleteBySelective " + clazz.getSimpleName() + ":" + entityToSerialString(entity));
		return getMapper().deleteBySelective(entity);
	}

	/**
	 * 更新
	 * 
	 */
	@Transactional
	@Override
	public int updateById(T entity) {
		logger.debug("updateByIdSelective " + clazz.getSimpleName() + ":" + entityToSerialString(entity));
		if (entity != null) {
			entity.setUpdateTime(DateUtils.getTimestamp());
			entity.setUpdateUser(getUserIdFromThreadData());
			entity.setUpdateUserName(getUserRealNameFromThreadData());
		}
		return getMapper().updateById(entity);
	}

	/**
	 * select by id
	 */
	@Override
	public T selectById(String id) {
		logger.debug("selectById " + clazz.getSimpleName() + ":" + id);
		return getMapper().selectById(id);
	}

	/**
	 * select by para
	 */
	public List<T> selectBySelective(T entity) {
		logger.debug("selectBySelective " + clazz.getSimpleName() + ":" + JsonUtils.beanToJson(entity));
		return getMapper().selectBySelective(entity);
	}

	/**
	 * select count by para
	 */
	@Override
	public Integer countBySelective(T entity) {
		logger.debug("countBySelective " + clazz.getSimpleName() + ":" + entityToSerialString(entity));
		return getMapper().countBySelective(entity);
	}

	/**
	 * entity to serialString
	 * 
	 * @param entity
	 * @return
	 */
	public String entityToSerialString(T entity) {
		if (entity != null) {
			return entity.toSerialString();
		}
		return null;
	}

	public String getUserIdFromThreadData() {
		return ThreadDataUtils.getUserId();
	}

	public String getUserNameFromThreadData() {
		return ThreadDataUtils.getUserName();
	}

	public String getUserRealNameFromThreadData() {
		return ThreadDataUtils.getUserRealName();
	}

	public String getUserMobileFromThreadData() {
		return ThreadDataUtils.getUserMobile();
	}
}
