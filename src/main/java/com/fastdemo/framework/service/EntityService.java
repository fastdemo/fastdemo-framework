package com.fastdemo.framework.service;

import java.util.List;

import com.fastdemo.framework.entity.BaseEntity;
import com.fastdemo.framework.entity.Search;
import com.fastdemo.framework.exception.system.FastDemoSystemException;

/**
 * 服务层基础接口
 * @param <T>
 */
public interface EntityService<T extends BaseEntity> {

	/**
	 * 数据导入接口
	 * 
	 * @param filePath
	 */
	public void importData(String filePath, String... params);

	/**
	 * 新增
	 * 
	 * @param entity
	 */
	public int insert(T entity);

	/**
	 * 新增或者更新
	 * 
	 * @param entity
	 */
	public int insertOrUpdate(T entity);

	/**
	 * 批量新增
	 * 
	 * @param entities
	 */
	public int insert(List<T> entities);

	/**
	 * 根据id删除 逻辑删除
	 * 
	 * @param id
	 */
	public int deleteById(String id);

	/**
	 * 批量删除 逻辑删除
	 * 
	 * @param ids
	 */
	public int deleteByIds(List<String> ids);

	/**
	 * 条件删除 逻辑删除
	 * 
	 * @param entity
	 */
	public int deleteBySelective(T entity);
	
	/**
	 * 根据id删除 物理删除
	 * 
	 * @param id
	 */
	public int realDeleteById(String id);

	/**
	 * 批量删除 物理删除
	 * 
	 * @param ids
	 */
	public int realDeleteByIds(List<String> ids);

	/**
	 * 条件删除 物理删除
	 * 
	 * @param entity
	 */
	public int realDeleteBySelective(T entity);

	/**
	 * 更新
	 * 
	 * @param entity
	 */
	public int updateById(T entity);

	/**
	 * 根据Entity ID 获取Entity
	 * 
	 * @param id
	 * @return
	 */
	public T selectById(String id);
	
	/**
	 * 根据Entity ID 批量更新
	 * 
	 * @param entities
	 */
	public int updateBatchByIds(List<T> entities) ;
	
	/**
	 * 根据Entity ID 批量更新 
	 * 
	 * @param data
	 * @param ids
	 */
	public int updateByIds(T data,List<String> ids);
	
	/**
	 * 根据Entity批量更新
	 * 
	 * @param query
	 * @param data
	 */
	public int updateBySelective(T query,T data);

	/**
	 * 根据Entity ID 获取Entity,用于联合主键查询
	 * 
	 * @param id
	 * @return
	 */
	public T queryOneBySelective(T entity) throws FastDemoSystemException;

	/**
	 * 分页查询Entity
	 * 
	 * @param page
	 * @return
	 */
	public Search<T> queryPageBySelective(Search<T> search);
	
	/**
	 * 分页导出
	 * @param search
	 * @return
	 */
	public Search<T> queryForExport(Search<T> search);

	/**
	 * 条件查询Entity
	 * 
	 * @param t
	 * @return
	 */
	public List<T> queryBySelective(Search<T> search);

	/**
	 * 条件查询Entity
	 * 
	 * @param t
	 * @return
	 */
	public List<T> queryBySelective(T entity);

	/**
	 * 条件查询数量
	 * 
	 * @param t
	 * @return
	 */
	public Integer countBySelective(T entity);

	/**
	 * 查询ID
	 * 
	 * @param entity
	 * @return
	 */
	public List<String> queryIdBySelective(T entity);
	
	/**
	 * 查询ID
	 * 
	 * @param entity
	 * @return
	 */
	public List<String> queryIdBySelective(Search<T> search);
	
	/**
	 * 根据id list查询
	 * 
	 * @param ids
	 * @return
	 */
	public List<T> queryByIds(List<String> ids);


	
}
