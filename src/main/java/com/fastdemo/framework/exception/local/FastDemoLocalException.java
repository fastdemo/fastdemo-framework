package com.fastdemo.framework.exception.local;

import com.fastdemo.framework.exception.FaseDemoException;

/**
 * 系统异常基类
 * 2017年11月10日
 * @author guxingchun
 */
public class FastDemoLocalException extends FaseDemoException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FastDemoLocalException(String message) {
		super(message);
	}
	
	public FastDemoLocalException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public FastDemoLocalException(String code, String message) {
		super(code, message);
	}
	public FastDemoLocalException(String code, String message, Throwable throwable) {
		super(message, code, throwable);
	}
}
