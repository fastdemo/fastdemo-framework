package com.fastdemo.framework.exception;

/**
 * projcet异常抽象类，所有 projcet抛出的异常都应该继承此类，以区分project和系统(依赖项)异常
 * 2017年11月10日
 * @author guxingchun
 */
public abstract class FaseDemoException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public FaseDemoException(String message) {
		super(message);
	}
	
	public FaseDemoException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public FaseDemoException(String code, String message) {
		super(message);
		this.code = code;
	}
	public FaseDemoException(String code, String message, Throwable throwable) {
		super(message, throwable);
		this.code = code;
	}
	
	protected String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
