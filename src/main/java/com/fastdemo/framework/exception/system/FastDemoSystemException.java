package com.fastdemo.framework.exception.system;

import com.fastdemo.framework.exception.FaseDemoException;

/**
 * 系统异常基类
 * 2017年11月10日
 * @author guxingchun
 */
public class FastDemoSystemException extends FaseDemoException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FastDemoSystemException(String message) {
		super(message);
	}
	
	public FastDemoSystemException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public FastDemoSystemException(String code, String message) {
		super(code, message);
	}
	public FastDemoSystemException(String code, String message, Throwable throwable) {
		super(message, code, throwable);
	}
}
