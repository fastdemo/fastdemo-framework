package com.fastdemo.framework.exception.remote;

import com.fastdemo.framework.exception.FaseDemoException;

/**
 * 系统异常基类
 * 2017年11月10日
 * @author guxingchun
 */
public class FastDemoRemoteException extends FaseDemoException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FastDemoRemoteException(String message) {
		super(message);
	}
	
	public FastDemoRemoteException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public FastDemoRemoteException(String code, String message) {
		super(code, message);
	}
	public FastDemoRemoteException(String code, String message, Throwable throwable) {
		super(message, code, throwable);
	}
}
