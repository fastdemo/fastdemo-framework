package com.fastdemo.framework.exception.business;

import com.fastdemo.framework.config.PropertyConfigurer;
import com.fastdemo.framework.exception.FaseDemoException;

/**
 * 系统异常基类
 * 2017年11月10日
 * @author guxingchun
 */
public class FastDemoBusinessException extends FaseDemoException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FastDemoBusinessException(String message) {
		super(message);
	}
	
	public FastDemoBusinessException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public FastDemoBusinessException(String code, String message) {
		super(code ,message);
	}
	public FastDemoBusinessException(String code, String message, Throwable throwable) {
		super(message, code, throwable);
	}
	
	public static FastDemoBusinessException codeException(String errorCode) {
		if(errorCode == null)
			return new FastDemoBusinessException(null);
		
		throw new FastDemoBusinessException(errorCode, PropertyConfigurer.getErrorMessage(errorCode));
	}
}
