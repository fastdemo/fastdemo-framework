package com.fastdemo.framework.config;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import com.fastdemo.framework.constant.ErrorCodeConstants;
import com.fastdemo.framework.exception.system.FastDemoSystemException;


public class PropertyConfigurer extends PropertyPlaceholderConfigurer {

	private static final Logger logger = LoggerFactory.getLogger(PropertyConfigurer.class);

	private static Map<String, String> ctxPropertiesMap = new HashMap<String, String>();
	private static Map<String, Map<String, String>> ctxPropertiesFieldMap = new HashMap<String, Map<String, String>>();
	
	private static Class<? extends Object>[] errorCodeClass = new Class<?>[] {};

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props)
			throws BeansException {
		super.processProperties(beanFactoryToProcess, props);
		setContexProperty(props);
	}

	public static void setContexProperty(Properties props) {
		if (ctxPropertiesMap == null) {
			ctxPropertiesMap = new HashMap<String, String>();
		}
		if (ctxPropertiesFieldMap == null) {
			ctxPropertiesFieldMap = new HashMap<String, Map<String, String>>();
		}
		String keyStr;
		String value;
		String oldValue;
		for (Object key : props.keySet()) {
			keyStr = key.toString();
			value = props.getProperty(keyStr);
			if (ctxPropertiesMap.containsKey(keyStr)) {
				oldValue = ctxPropertiesMap.get(keyStr);
				logger.warn("property [key:" + keyStr + ",value:" + oldValue + "] override by [key:" + keyStr
						+ ",value:" + value + "]");
			}
			ctxPropertiesMap.put(keyStr, value);
		}
	}

	public static String getSimpleErrorMessage(String code) {
		String message = ctxPropertiesMap.get(code);
		if (message == null) {
			logger.warn("property [key:" + code + "] not exist");
			return null;
		}
		return message;
	}

	public static String getErrorMessage(String code) {
		String message = ctxPropertiesMap.get(code);
		if (message == null) {
			logger.warn("property [key:" + code + "] not exist,there may be throw exception");
			return null;
		}
		return code + ":" + message;
	}

	public static String getErrorMessage(String code, Object... args) {
		String message = ctxPropertiesMap.get(code);
		if (message == null) {
			logger.warn("property [key:" + code + "] not exist,there may be throw exception");
			return null;
		}
		return code + ":" + MessageFormat.format(message, args);
	}

	public static void resetProperty(String code, String value) {
		if (StringUtils.isBlank(code) || StringUtils.isBlank(value)) {
			logger.error("property [key:" + code + ",value:" + value + "] reset error");
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_PROPERTY_BLANK;
			throw new FastDemoSystemException(errorCode, getErrorMessage(errorCode));
		}
		if (!ctxPropertiesMap.containsKey(code)) {
			logger.error("property [key:" + code + "] not exist");
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_PROPERTY_NOT_EXIST;
			throw new FastDemoSystemException(errorCode, getErrorMessage(errorCode));
		}
		ctxPropertiesMap.put(code, value);
		logger.info("property [key:" + code + ",value:" + value + "] reset success");
	}

	public static String getValue(String code) {
		String value = ctxPropertiesMap.get(code);
		if (value == null) {
			logger.warn("property [key:" + code
					+ "] not exist,there may be throw exception");
			return null;
		}
		return value;
	}
	
	/**
	 * 获取errorCode对应的字段名
	 * @param errorCode
	 * @param errorClass errorCode所在的常量类
	 * @return
	 * @date 2018年1月9日
	 * @author guxingchun
	 */
	public static String getFieldName(String errorCode, Class<?> errorClass) {
		String fieldName = null;
		
		if(errorClass == null || errorCode == null)
			return null;
		String fieldErrorCode;
		boolean search = false;
		Map<String, String> fileNameMap;
		for (Class<? extends Object> errorClazz : errorCodeClass) {
			if(errorClass.getCanonicalName().equals(errorClazz.getCanonicalName())) {
				search = true;
				fileNameMap = ctxPropertiesFieldMap.get(errorClass.getCanonicalName());
				fieldName = fileNameMap.get(errorCode);
				return fieldName;
				
			}
		}
		
		if(!search) {
			fileNameMap = new HashMap<String, String>();
			ctxPropertiesFieldMap.put(errorClass.getCanonicalName(), fileNameMap);
			for (Field field : errorClass.getFields()) {
				try {
					fieldErrorCode = field.get(errorClass).toString();
					fileNameMap.put(fieldErrorCode, field.getName());
					if(errorCode.equals(fieldErrorCode))
						fieldName = field.getName();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return fieldName;
	}

}
