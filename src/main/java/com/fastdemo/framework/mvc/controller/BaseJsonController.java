package com.fastdemo.framework.mvc.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;

import com.fastdemo.framework.config.PropertyConfigurer;
import com.fastdemo.framework.constant.ApplicationStatusConstants;
import com.fastdemo.framework.constant.ErrorCodeConstants;
import com.fastdemo.framework.constant.ThreadDataKeyConstants;
import com.fastdemo.framework.constant.UserParamConstans;
import com.fastdemo.framework.exception.FaseDemoException;
import com.fastdemo.framework.exception.system.FastDemoSystemException;
import com.fastdemo.framework.utils.ResultUtils;
import com.fastdemo.framework.utils.ThreadDataUtils;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * 2018年1月12日
 * @author guxingchun
 */
public abstract class BaseJsonController {

	private static final Logger logger = LoggerFactory
			.getLogger(BaseJsonController.class);

	/**
	 * 获取当前登录的用户Id
	 * 
	 * @return
	 */
	protected String getUserIdFromThreadData() {
		Map<String, Object> userInfoMap = getUserMapFromThreadData();
		Object oUserId = userInfoMap.get(UserParamConstans.KEY_USER_ID);
		if (oUserId == null) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_THREAD_DATA_USER_ID_NOT_EXIST;
			throw new FastDemoSystemException(errorCode,
					PropertyConfigurer.getErrorMessage(errorCode));
		}
		return (String) oUserId;
	}
	
//	protected UserInfoSupport getUserInfoFromThreadData() {
//		
//		UserInfoSupport us = new UserInfoSupport();
//		Map<String, Object> userInfoMap = getUserMapFromThreadData();
//		Object oUserId = userInfoMap.get(UserParamConstans.KEY_USER_ID);
//		if (oUserId == null) {
//			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_THREAD_DATA_USER_ID_NOT_EXIST;
//			throw new PurangSystemException(errorCode,
//					PropertyConfigurer.getErrorMessage(errorCode));
//		}
//		Object userRealNameObj = userInfoMap.get(UserParamConstans.KEY_USER_REAL_NAME);
//		Object userNameObj = userInfoMap.get(UserParamConstans.KEY_USER_NAME);
//		String userNameStr = "";
//
//		//用户名/昵称
//		if(userNameObj != null){
//			userNameStr = userNameObj.toString();
//		}
//
//		//真实姓名为空则赋值用户名或昵称
//		if(userRealNameObj == null){
//			us.setUserRealName(userNameStr);
//		} else {
//			//否则直接赋值真实姓名
//			us.setUserRealName(String.valueOf(userInfoMap.get(UserParamConstans.KEY_USER_REAL_NAME)));
//		}
//
//		us.setUserName(userNameStr);
//		us.setUserId(oUserId.toString());
//		us.setUserType((userInfoMap.get(UserParamConstans.KEY_USER_TYPE)) == null ? null : Integer.parseInt(userInfoMap.get(UserParamConstans.KEY_USER_TYPE).toString()));
//		us.setCompanySuperAdmin(userInfoMap.get(UserParamConstans.SUPER_ADMIN) !=null && Boolean.parseBoolean(userInfoMap.get(UserParamConstans.SUPER_ADMIN).toString()));
//
//
//		//部门信息
//		if(userInfoMap.containsKey(UserParamConstans.ACTUAL_DEPARTMENT) && userInfoMap.get(UserParamConstans.ACTUAL_DEPARTMENT) != null){
//			Map<String, Object> deptMap = JsonUtils.jsonToMap(String.valueOf(userInfoMap.get(UserParamConstans.ACTUAL_DEPARTMENT)), String.class, Object.class);
//			us.setActualDeptName(String.valueOf(deptMap.get(UserParamConstans.KEY_DEPT_NAME)));
//			us.setActualDeptId(String.valueOf(deptMap.get(UserParamConstans.KEY_DEPT_ID)));
//		}
//
//		return us;
//	}

	/**
	 * 获取当前登录的用户Map
	 * 
	 * @return
	 */
	protected Map<String, Object> getUserMapFromThreadData() {
		Map<String, Object> userInfoMap = ThreadDataUtils
				.getMapThreadData(ThreadDataKeyConstants.THREAD_DATA_KEY_USER_INFOR_MAP);
		if (userInfoMap == null) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_THREAD_DATA_USER_NOT_EXIST;
			throw new FastDemoSystemException(errorCode,
					PropertyConfigurer.getErrorMessage(errorCode));
		}
		return (Map<String, Object>) userInfoMap;
	}
	
	protected void setApplicationStatus(int applicationStatus) {
//		Object oCellMessage = (HttpRequestLog) ThreadDataUtils
//				.getThreadData(ThreadDataKeyConstants.THREAD_DATA_KEY_CELL_LOG);
//		if(oCellMessage != null){
//			HttpRequestLog cellMessage = (HttpRequestLog)oCellMessage;
//			cellMessage.setApplicationStatus(applicationStatus);
//		}
	}

	/**
	 * 捕捉业务异常
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler({ FaseDemoException.class })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	protected Map<String, Object> handleDecException(FaseDemoException ex) {
		logger.error("BaseJsonController handleDecException:", ex);
		return ResultUtils.createPurangExceptionResult(ex);
	}
	
	/**
	 * Http请求消息体为空
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler({ HttpMessageNotReadableException.class })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	protected Map<String, Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
		logger.error("BaseJsonController httpMessageNotReadableException:", ex);
		setApplicationStatus(ApplicationStatusConstants.APP_STATUS_EXCEPTION_HTTP_MESSAGE_NOT_READABLE);
		return ResultUtils.createHttpMessageNotReadableExceptionResult(ex);
	}
	
	/**
	 * 捕捉MVC校验异常
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler({ MethodArgumentNotValidException.class })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	protected Map<String, Object> handleValidException(
			MethodArgumentNotValidException ex) {
		logger.error("BaseJsonController MethodArgumentNotValidException:", ex);
		return ResultUtils.createMethodArgumentNotValidExceptionResult(ex);
	}
	
	/**
	 * 捕捉文件异常
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler({ MultipartException.class })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	protected Map<String, Object> handleMultipartException(
			MultipartException ex) {
		logger.error("BaseJsonController MultipartException:", ex);
		setApplicationStatus(ApplicationStatusConstants.APP_STATUS_EXCEPTION_MULTIPART);
		return ResultUtils.createMultipartExceptionResult(ex);
	}
	
	/**
	 * 捕捉文件异常
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler({ MaxUploadSizeExceededException.class })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	protected Map<String, Object> handleMaxUploadSizeExceededException(
			MaxUploadSizeExceededException ex) {
		logger.error("BaseJsonController MaxUploadSizeExceededException:", ex);
		setApplicationStatus(ApplicationStatusConstants.APP_STATUS_EXCEPTION_MAX_UPLOAD_SIZE_EXCEEDED);
		return ResultUtils.createMaxUploadSizeExceededExceptionResult(ex);
	}
	
	/**
	 * 捕捉MVC校验异常
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler({ BindException.class })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	protected Map<String, Object> handleValidException(BindException ex) {
		logger.error("BaseJsonController BindException:", ex);
		return ResultUtils.createBindExceptionResult(ex);
	}

	/**
	 * 捕捉JSON异常
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler({ JsonProcessingException.class })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	protected Map<String, Object> handleJsonException(JsonProcessingException ex) {
		logger.error("BaseJsonController JsonProcessingException:", ex);
		setApplicationStatus(ApplicationStatusConstants.APP_STATUS_EXCEPTION_JSON_PROCESSING);
		return ResultUtils.createJsonProcessingExceptionResult(ex);
	}

	/**
	 * HTTP消息转换异常
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler({ HttpMessageConversionException.class })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	protected Map<String, Object> handleHttpMessageConversionException(
			HttpMessageConversionException ex) {
		logger.error("BaseJsonController JsonProcessingException:", ex);
		setApplicationStatus(ApplicationStatusConstants.APP_STATUS_EXCEPTION_HTTP_MESSAGE_COVERSION);
		return ResultUtils.createHttpMessageConversionExceptionResult(ex);
	}

	/**
	 * 捕捉除业务异常外其他所有异常
	 * 
	 * @param ex
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	protected Map<String, Object> handleCommonException(Exception ex) {
		logger.error("BaseJsonController handleCommonException:", ex);
		setApplicationStatus(ApplicationStatusConstants.APP_STATUS_EXCEPTION_UN_KNOW);
		return ResultUtils.createUnknowExceptionResult(ex);
	}

}
