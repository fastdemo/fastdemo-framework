package com.fastdemo.framework.mvc.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fastdemo.framework.annotation.Security;
import com.fastdemo.framework.config.PropertyConfigurer;
import com.fastdemo.framework.constant.ErrorCodeConstants;
import com.fastdemo.framework.dto.sys.PropertyDTO;
import com.fastdemo.framework.utils.ResultUtils;

/**
 * 该类主要用于系统运行状态监控
 * 
 * @author wangchaochao
 *
 */
@Controller
@RequestMapping("/systemManager/v1")
public class SystemManagerController extends BaseJsonController {

	/**
	 * reset 系统配置
	 */
	@RequestMapping(value = "/resetProperty.do")
	@ResponseBody
	@Security(checkLogon = true)
	public Map<String, Object> resetProperty(
			@RequestBody @Valid PropertyDTO propertyDTO,
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {
		PropertyConfigurer.resetProperty(propertyDTO.getCode(),
				propertyDTO.getValue());
		return ResultUtils
				.createSuccessResult(ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_PROPERTY_WRAN);
	}

	/**
	 * query 系统配置
	 */
	@RequestMapping(value = "/queryProperty.do")
	@ResponseBody
	@Security(checkLogon = true)
	public Map<String, Object> queryProperty(
			@RequestBody PropertyDTO propertyDTO,
			HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {
		String value = PropertyConfigurer.getValue(propertyDTO.getCode());
		propertyDTO.setValue(value);
		return ResultUtils.createDtoResult(propertyDTO);
	}


}
