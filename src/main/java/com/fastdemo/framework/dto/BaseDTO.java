package com.fastdemo.framework.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fastdemo.framework.constant.IsDeleteConstants;
import com.fastdemo.framework.utils.JsonUtils;

/**
 * 所有entityDTO基类
 * 
 */
@JsonInclude(Include.NON_NULL)
public class BaseDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 物理主键.
	 */
	private String id;

	/**
	 * 创建时间 COLUMN:CREATE_TIME.
	 */
	private String createTime;

	/**
	 * 更新时间 COLUMN:UPString_TIME.
	 */
	private String updateTime;

	/**
	 * 创建人ID COLUMN:CREATE_USER.
	 */
	private String createUser;

	/**
	 * 更新人ID COLUMN:UPString_USER.
	 */
	private String updateUser;
	
	/**
	 * 创建人NAME COLUMN:CREATE_USER_NAME.
	 */
	private String createUserName;

	/**
	 * 更新人NAME COLUMN:UPString_USER_NAME.
	 */
	private String updateUserName;
	
	/**
	 * 版本 COLUMN:VERSION.
	 */
	private Integer version;

	/**
	 * 删除标记 COLUMN:IS_DELETE.
	 */
	private Integer isDelete = IsDeleteConstants.DELETE_OFF;
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String toSerialString(){
		return JsonUtils.beanToJson(this);
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getUpdateUserName() {
		return updateUserName;
	}

	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

}
