package com.fastdemo.framework.dto;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author wangchaochao
 *
 * @param <T>
 */
@JsonInclude(Include.NON_NULL)
public class PageDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 当前页码,从1开始
	 */
	@NotNull(message = "{PageDTO.pageNo.NotNull}")
	@Min(message = "PageDTO.pageNo.Min.One", value = 1)
	private Integer pageNo = 1;

	/**
	 * 一页个数,默认为10
	 */
	@NotNull(message = "{PageDTO.pageSize.NotNull}")
	@Min(message = "PageDTO.pageSize.Min.One", value = 1)
	private Integer pageSize = 10;

	/**
	 * 当前个数
	 */
	private Integer currentSize;

	/**
	 * 页数
	 */
	private Integer pageCount;

	/**
	 * 总记录数
	 */
	private Integer totalCount;

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageCount() {
		return pageCount;
	}

	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getCurrentSize() {
		return currentSize;
	}

	public void setCurrentSize(Integer currentSize) {
		this.currentSize = currentSize;
	}

}
