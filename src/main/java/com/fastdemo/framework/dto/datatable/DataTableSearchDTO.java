package com.fastdemo.framework.dto.datatable;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fastdemo.framework.dto.BaseDTO;
import com.fastdemo.framework.dto.OrderDTO;
import com.fastdemo.framework.dto.PageDTO;
import com.fastdemo.framework.utils.StringUtil;

@JsonInclude(Include.NON_NULL)
public class DataTableSearchDTO<T extends BaseDTO> {

	public static final String SEARCH_NAME = "dataTableSearch";

	private String draw;

	private List<Column> columns;

	private List<Order> order;

	private Integer start;

	private Integer length;

	private Search search;

	private T queryParams;

	private List<T> entities;

	public String getDraw() {
		return draw;
	}

	public void setDraw(String draw) {
		this.draw = draw;
	}

	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public List<Order> getOrder() {
		return order;
	}

	public void setOrder(List<Order> order) {
		this.order = order;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Search getSearch() {
		return search;
	}

	public void setSearch(Search search) {
		this.search = search;
	}

	public T getQueryParams() {
		return queryParams;
	}

	public void setQueryParams(T queryParams) {
		this.queryParams = queryParams;
	}

	public List<T> getEntities() {
		return entities;
	}

	public void setEntities(List<T> entities) {
		this.entities = entities;
	}

	public com.fastdemo.framework.dto.SearchDTO<T> convertSearch() {

		com.fastdemo.framework.dto.SearchDTO<T> search = new com.fastdemo.framework.dto.SearchDTO<T>();

		PageDTO page = new PageDTO();
		if (getStart() == null)
			page.setPageNo(1);
		else
			page.setPageNo(1 + getStart() / getLength());
		page.setPageSize(getLength());
		page.setCurrentSize(getLength());
		search.setPageDTO(page);
		
		
		List<Column> columns = getColumns();
		if(CollectionUtils.isNotEmpty(getOrder()) && CollectionUtils.isNotEmpty(columns)) {
			List<OrderDTO> orderDTOs = new ArrayList<OrderDTO>();
			OrderDTO orderDTO = null;
			Integer columnIndex;
			String dir;
			Column column;
			for (Order order : getOrder()) {
				columnIndex = order.getColumn();
				dir = order.getDir();
				column = columns.get(columnIndex);
				
				if(StringUtil.isNotEmpty(dir)) {
					orderDTO = new OrderDTO();
					orderDTO.setPropertyName(column.getData());
					orderDTO.setDir(dir);
					orderDTOs.add(orderDTO);
				}
			}
			
			search.setOrderDTOs(orderDTOs);
		}

		search.setEntityDTO(getQueryParams());
		return search;
	}
}
