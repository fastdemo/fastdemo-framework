package com.fastdemo.framework.dto.sys;

import org.hibernate.validator.constraints.NotBlank;

import com.fastdemo.framework.dto.BaseDTO;

public class PropertyDTO extends BaseDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotBlank(message = "{PropertyDTO.code.NotNull}")
	private String code;
	
	@NotBlank(message = "{PropertyDTO.value.NotNull}")
	private String value;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
