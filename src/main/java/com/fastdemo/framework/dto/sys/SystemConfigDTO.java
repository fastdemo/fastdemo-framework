package com.fastdemo.framework.dto.sys;

import com.fastdemo.framework.dto.BaseDTO;

/**
 * @Description auto create by dec-tool-V1.0
 * 
 * @author 亚飞
 * @date 2015-10-20 15:35:12
 * 
 */
public class SystemConfigDTO extends BaseDTO {
    
    /**
     * SerialVersion
     */
    private static final long serialVersionUID = 1L;

    /**
     * 系统配置名 COLUMN:CONFIG_NAME
     */
    private String configName;

    /**
     * 系统配置CODE COLUMN:CONFIG_CODE
     */
    private String configCode;

    /**
     * 系统配置描述 COLUMN:CONFIG_DESC
     */
    private String configDesc;

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getConfigCode() {
        return configCode;
    }

    public void setConfigCode(String configCode) {
        this.configCode = configCode;
    }

    public String getConfigDesc() {
        return configDesc;
    }

    public void setConfigDesc(String configDesc) {
        this.configDesc = configDesc;
    }
}