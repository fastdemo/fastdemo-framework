package com.fastdemo.framework.dto.sys;

import com.fastdemo.framework.dto.BaseDTO;

/**
 * @Description auto create by dec-tool-V1.0
 * 
 * @author 亚飞
 * @date 2015-10-20 15:35:38
 * 
 */
public class SystemConfigDataDTO extends BaseDTO {
    
    /**
     * SerialVersion
     */
    private static final long serialVersionUID = 1L;

    /**
     * 系统配置ID COLUMN:CONFIG_ID
     */
    private String configId;

    /**
     * 系统配置-数据CODE COLUMN:CONFIG_DATA_CODE
     */
    private String configDataCode;

    /**
     * 系统配置-数据值 COLUMN:CONFIG_DATA_VALUE
     */
    private String configDataValue;

    /**
     * 系统配置-数据父节点 COLUMN:CONFIG_DATA_PARENT
     */
    private String configDataParent;

    /**
     * 系统配置-数据描述 COLUMN:CONFIG_DATA_DESC
     */
    private String configDataDesc;

    /**
     * 是否启用，0-启用 1-不启用 COLUMN:IS_START_USING
     */
    private Integer isStartUsing;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getConfigDataCode() {
        return configDataCode;
    }

    public void setConfigDataCode(String configDataCode) {
        this.configDataCode = configDataCode;
    }

    public String getConfigDataValue() {
        return configDataValue;
    }

    public void setConfigDataValue(String configDataValue) {
        this.configDataValue = configDataValue;
    }

    public String getConfigDataParent() {
        return configDataParent;
    }

    public void setConfigDataParent(String configDataParent) {
        this.configDataParent = configDataParent;
    }

    public String getConfigDataDesc() {
        return configDataDesc;
    }

    public void setConfigDataDesc(String configDataDesc) {
        this.configDataDesc = configDataDesc;
    }

    public Integer getIsStartUsing() {
        return isStartUsing;
    }

    public void setIsStartUsing(Integer isStartUsing) {
        this.isStartUsing = isStartUsing;
    }
}