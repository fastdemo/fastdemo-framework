package com.fastdemo.framework.dto;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 外部接口请求对象
 * 
 * @author wangchaochao
 *
 * @param <T>
 */
@JsonInclude(Include.NON_NULL)
public class SearchDTO<T extends BaseDTO> {
	
	/**
	 * 请求ID,唯一标示一次请求
	 */
	private String requestId;
	
	/**
	 * 查询条件
	 */
	private T entityDTO;
	
	/**
	 * entity id 集合,用于批量操作
	 */
	private List<String> ids;

	/**
	 * page 条件
	 */
	private PageDTO pageDTO;
	
	/**
	 * 排序条件
	 */
	private List<OrderDTO> orderDTOs;
	
	/**
	 * 其他扩展条件
	 */
	private Map<String,String> extParams;
	
	/**
	 * 返回结果
	 */
	private List<T> records;

	public PageDTO getPageDTO() {
		return pageDTO;
	}

	public void setPageDTO(PageDTO pageDTO) {
		this.pageDTO = pageDTO;
	}

	public List<OrderDTO> getOrderDTOs() {
		return orderDTOs;
	}

	public void setOrderDTOs(List<OrderDTO> orderDTOs) {
		this.orderDTOs = orderDTOs;
	}

	public T getEntityDTO() {
		return entityDTO;
	}

	public void setEntityDTO(T entityDTO) {
		this.entityDTO = entityDTO;
	}

	public List<T> getRecords() {
		return records;
	}

	public void setRecords(List<T> records) {
		this.records = records;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

	public Map<String, String> getExtParams() {
		return extParams;
	}

	public void setExtParams(Map<String, String> extParams) {
		this.extParams = extParams;
	}
	
	
}
