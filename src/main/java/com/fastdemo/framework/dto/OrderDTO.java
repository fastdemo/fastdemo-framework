package com.fastdemo.framework.dto;

import org.hibernate.validator.constraints.NotBlank;

public class OrderDTO {

	@NotBlank(message = "{OrderDTO.propertyName.NotBlank}")
	private String propertyName;
	
	@NotBlank(message = "{OrderDTO.dir.NotBlank}")
	private String dir;

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}
	
	
	
}
