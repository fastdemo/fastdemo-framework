package com.fastdemo.framework.mapper;

import java.util.List;
import java.util.Map;

import com.fastdemo.framework.entity.BaseEntity;

public interface EntityMapper<T extends BaseEntity> extends BaseMapper<T> {

	/**
	 * 直接执行SQL<br>
	 * 
	 * 该方法不会被分页切面拦截
	 * 
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> executeCmd(String cmd);
	
	

}
