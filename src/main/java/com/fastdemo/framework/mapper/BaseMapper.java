package com.fastdemo.framework.mapper;

import java.util.List;
import java.util.Map;

import com.fastdemo.framework.entity.BaseEntity;


public interface BaseMapper<T extends BaseEntity> {
	
	/**
	 * 新增
	 * 
	 * @param entity
	 * @return
	 */
	int insert(T entity);
	
	/**
	 * 新增
	 * 
	 * @param entity
	 * @return
	 */
	int insertBatch(List<T> entities);
	
	/**
	 * 根据主键删除
	 * 
	 * @param id
	 * @return
	 */
	int deleteById(String id);

	/**
	 * 根据主键删除
	 * 
	 * @param ids
	 * @return
	 */
	int deleteBatchById(List<String> ids);
	
	/**
	 * 条件删除
	 * 
	 * @param id
	 * @return
	 */
	int deleteBySelective(T entity);

	/**
	 * 条件主键更新
	 * 
	 * @param entity
	 * @return
	 */
	int updateById(T entity);
	
	/**
	 * 根据id批量更新
	 * 
	 * @param entity
	 * @return
	 */
	int updateByIds(Map<String,Object> map);
	
	/**
	 * 批量条件主键更新
	 * 
	 * @param entity
	 * @return
	 */
	int updateBatchById(List<T> entity);
	
	/**
	 * 批量条件主键更新
	 * 
	 * @param entity
	 * @return
	 */
	int updateBySelective(T entity);
	
	/**
	 * 根据主键查询
	 * 
	 * @param id
	 * @return
	 */
	T selectById(String id);

	/**
	 * 查询数量
	 * 
	 * @param record
	 * @return
	 */
	int countBySelective(T entity);

	/**
	 * 条件查询
	 * 
	 * @param map
	 * @return
	 */
	List<T> selectBySelective(T entity);
	
	/**
	 * 条件查询
	 * 
	 * @param map
	 * @return
	 */
	List<String> selectIdBySelective(T entity);
	
	/**
	 * 条件查询
	 * 
	 * @param map
	 * @return
	 */
	List<T> selectByIds(List<String> ids);
	
}
