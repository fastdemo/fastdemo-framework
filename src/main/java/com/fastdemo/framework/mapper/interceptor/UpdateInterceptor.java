package com.fastdemo.framework.mapper.interceptor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.ParameterMode;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.scripting.defaults.DefaultParameterHandler;
import org.apache.ibatis.session.defaults.DefaultSqlSession.StrictMap;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fastdemo.framework.config.PropertyConfigurer;
import com.fastdemo.framework.constant.ErrorCodeConstants;
import com.fastdemo.framework.utils.MatchUtils;

//只拦截update部分
@Intercepts({ @Signature(type = Executor.class, method = "update", args = { MappedStatement.class, Object.class }) })
public class UpdateInterceptor implements Interceptor {

	/**
	 * UpdateInterceptor log
	 */
	private static final Logger logger = LoggerFactory.getLogger(UpdateInterceptor.class);

	private static final String KEY_LIST = "list";

	private List<String> methodNames;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		final Object[] args = invocation.getArgs();
		MappedStatement mappedStatement = (MappedStatement) args[0];
		if (!isMatch(mappedStatement.getId())) {
			return invocation.proceed();
		}

		List<String> sqlLogs = new ArrayList<String>();
		List<String> paramLogs = new ArrayList<String>();
		List<String> resultLogs = new ArrayList<String>();

		Executor executor = (Executor) invocation.getTarget();
		Connection connection = executor.getTransaction().getConnection();
		SqlSource sqlSource = mappedStatement.getSqlSource();
		StrictMap strictMap = (StrictMap) args[1];
		List list = (List) strictMap.get(KEY_LIST);
		int count = list.size();
		Statement statement = connection.createStatement();
		boolean flag = false;
		for (int i = 0; i < list.size(); i++) {
			List subList = new ArrayList();
			subList.add(list.get(i));
			strictMap.put(KEY_LIST, subList);
			BoundSql boundSql = sqlSource.getBoundSql(args[1]);
			String sql = boundSql.getSql();
			// 带参数的原始sql
			sql = removeBreakingWhitespace(sql);

			PreparedStatement pstmt = connection.prepareStatement(sql);
			if (flag) {
				sqlLogs.add("==>    Preparing: (" + i + ") " + sql);
			} else {
				sqlLogs.add("==>    Preparing: (" + i + ") " + sql);
			}
			ParameterHandler parameterHandler = new DefaultParameterHandler(mappedStatement, boundSql.getParameterObject(), boundSql);
			paramLogs.add("==>   Parameters: (" + i + ") " + buildParametersLog(mappedStatement, boundSql));
			parameterHandler.setParameters(pstmt);
			// 解析后的sql
			String resultSql = pstmt.toString();
			resultSql = pstmt.toString().substring(resultSql.indexOf(":") + 1, resultSql.length()).trim();
			statement.addBatch(resultSql);
			flag = true;
		}
		int[] results = statement.executeBatch();
		int result = 0;
		if (results != null) {
			for (int subResult : results) {
				resultLogs.add("<==      Updates: " + subResult);
				result = result + subResult;
			}
		}
		for (int i = 0; i < count; i++) {
			logger.debug(sqlLogs.get(i));
			logger.debug(paramLogs.get(i));
			logger.debug(resultLogs.get(i));
		}
		logger.debug("<== TotalUpdates: " + result);
		if (result != count) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_DB_BATCH_UPDATE_ERROR;
			logger.warn(PropertyConfigurer.getErrorMessage(errorCode));
			// throw new PurangSystemException(errorCode,
			// PropertyConfigurer.getErrorMessage(errorCode));
		}
		return result;
	}

	private String buildParametersLog(MappedStatement mappedStatement, BoundSql boundSql) {
		Object parameterObject = boundSql.getParameterObject();
		List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
		TypeHandlerRegistry typeHandlerRegistry = mappedStatement.getConfiguration().getTypeHandlerRegistry();
		StringBuilder sb = new StringBuilder();
		if (parameterMappings != null && parameterMappings.size() > 0) {
			boolean flag = false;
			for (ParameterMapping parameterMapping : parameterMappings) {
				if (flag) {
					sb.append(",");
				}

				Object value = null;
				String propertyName = parameterMapping.getProperty();
				if (parameterMapping.getMode() != ParameterMode.OUT) {
					if (boundSql.hasAdditionalParameter(propertyName)) {
						value = boundSql.getAdditionalParameter(propertyName);
					} else if (parameterObject == null) {
						value = null;
					} else if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
						value = parameterObject;
					} else {
						MetaObject metaObject = mappedStatement.getConfiguration().newMetaObject(parameterObject);
						value = metaObject.getValue(propertyName);
					}
				}
				if (value == null) {
					sb.append("null");
				} else {
					sb.append(value.toString());
				}
				sb.append("(");
				sb.append(propertyName.substring(propertyName.lastIndexOf(".") + 1));
				sb.append(":");
				sb.append(parameterMapping.getJdbcType());
				sb.append(")");
				flag = true;
			}
		}
		return sb.toString();
	}

	private String removeBreakingWhitespace(String original) {
		StringTokenizer whitespaceStripper = new StringTokenizer(original);
		StringBuilder builder = new StringBuilder();
		while (whitespaceStripper.hasMoreTokens()) {
			builder.append(whitespaceStripper.nextToken());
			builder.append(" ");
		}
		return builder.toString();
	}

	@Override
	public Object plugin(Object target) {
		if (target instanceof Executor) {
			return Plugin.wrap(target, this);
		} else {
			return target;
		}
	}

	@Override
	public void setProperties(Properties properties) {
	}

	private boolean isMatch(String methodName) {
		if (methodNames == null || methodNames.size() <= 0) {
			return false;
		}
		for (String mtName : methodNames) {
			if (MatchUtils.match(mtName, methodName)) {
				return true;
			}
		}
		return false;
	}

	public List<String> getMethodNames() {
		return methodNames;
	}

	public void setMethodNames(List<String> methodNames) {
		this.methodNames = methodNames;
	}

}
