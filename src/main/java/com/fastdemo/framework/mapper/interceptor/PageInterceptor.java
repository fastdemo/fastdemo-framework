package com.fastdemo.framework.mapper.interceptor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.builder.StaticSqlSource;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.mapping.ResultMapping;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.factory.ObjectFactory;
import org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory;
import org.apache.ibatis.scripting.defaults.DefaultParameterHandler;
import org.apache.ibatis.scripting.xmltags.DynamicSqlSource;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.TextSqlNode;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import com.fastdemo.framework.config.PropertyConfigurer;
import com.fastdemo.framework.constant.ErrorCodeConstants;
import com.fastdemo.framework.entity.Order;
import com.fastdemo.framework.entity.Page;
import com.fastdemo.framework.exception.system.FastDemoSystemException;
import com.fastdemo.framework.utils.MatchUtils;
import com.fastdemo.framework.utils.SqlUtils;

//只拦截select部分
@Intercepts({ @Signature(type = Executor.class, method = "query", args = {
		MappedStatement.class, Object.class, RowBounds.class,
		ResultHandler.class }) })
public class PageInterceptor implements Interceptor {

	public static final String FLAG_SEARCH = "search";

	public static final String FLAG_COUNT = "count";

	public static final String DIALECT_ORACLE = "oracle";

	public static final String DIALECT_MYSQL = "mysql";

	public static final ObjectFactory DEFAULT_OBJECT_FACTORY = new DefaultObjectFactory();

	public static final ObjectWrapperFactory DEFAULT_OBJECT_WRAPPER_FACTORY = new DefaultObjectWrapperFactory();

	public static final MetaObject NULL_META_OBJECT = MetaObject.forObject(
			NullObject.class, DEFAULT_OBJECT_FACTORY,
			DEFAULT_OBJECT_WRAPPER_FACTORY);

	private static final String countStartSql = "select count(0) as total from (";
	private static final String countEndSql = ") t";
	private static final String pageStartSql = "select * from ( select temp.*, rownum row_id from ( ";
	private static final String pageMidSql = " ) temp where rownum <= ";
	private static final String pageEndSql = ") where row_id > ";

	private static final TextSqlNode countStartSqlNode = new TextSqlNode(
			countStartSql);
	private static final TextSqlNode countEndSqlNode = new TextSqlNode(
			countEndSql);
	private static final TextSqlNode pageStartSqlNode = new TextSqlNode(
			pageStartSql);
	private static final List<ResultMapping> EMPTY_RESULTMAPPING = new ArrayList<ResultMapping>(
			0);

	private String dialect;

	private String defaultDialect = DIALECT_ORACLE;

	private List<String> methodNames;

	private Boolean limitFlag = true; // 是否添加limit尾缀

	@SuppressWarnings({ "unchecked" })
	public Object intercept(Invocation invocation) throws Throwable {
		final Object[] args = invocation.getArgs();
		limitFlag = true;
		MappedStatement mappedStatement = (MappedStatement) args[0];

		if (isMatch(mappedStatement.getId())) {
			Map<String, Object> paramMap = (Map<String, Object>) args[1];
			// 只重写需要分页的sql语句。通过MappedStatement的ID匹配，默认重写以Page结尾的
			// MappedStatement的sql
			SqlSource sqlSource = mappedStatement.getSqlSource();
			BoundSql boundSql = sqlSource.getBoundSql(args[1]);
			Object parameterObject = boundSql.getParameterObject();

			// 分页参数作为参数对象parameterObject的一个属性
			Object pageObj = paramMap.get(Page.PAGE_NAME);
			Object orderObjs = paramMap.get(Order.ORDER_NAME);
			List<Order> orders = null;
			if (orderObjs != null) {
				orders = (List<Order>) orderObjs;
			}
			if (pageObj == null && (orders == null || orders.size() <= 0)) {
				return invocation.proceed();
			}

			Page page = null;
			String sql = boundSql.getSql();
			String staticSql = sql;
			Pattern pat = Pattern.compile("[\\s\\S]* limit [\\s\\S]*");
			if (pat.matcher(sql.toLowerCase()).matches()) {
				limitFlag = false;// 如果语句中包含limit，则不在最后加limit后缀
			}
			List<ResultMapping> resultMappings = mappedStatement
					.getResultMaps().get(0).getResultMappings();
			Map<String, String> resultMappingMap = converResultMappingList2Map(resultMappings);

			if (pageObj != null) {
				page = (Page) pageObj;
				if (!limitFlag) {
					sql = buildLimitForMysql(sql, page, FLAG_COUNT);
					sqlSource = new StaticSqlSource(
							mappedStatement.getConfiguration(), sql,
							boundSql.getParameterMappings());
				}

				String countSql = SqlUtils.buildCountSql(sql);
				Connection connection = mappedStatement.getConfiguration()
						.getEnvironment().getDataSource().getConnection();
				PreparedStatement countStmt = connection
						.prepareStatement(countSql);
				BoundSql countBS = copyFromBoundSql(mappedStatement, boundSql,
						countSql);
				DefaultParameterHandler parameterHandler = new DefaultParameterHandler(
						mappedStatement, parameterObject, countBS);
				parameterHandler.setParameters(countStmt);
				ResultSet rs = countStmt.executeQuery();
				Integer totalCount = 0;
				if (rs.next()) {
					totalCount = rs.getInt(1);
				}
				rs.close();
				countStmt.close();
				connection.close();

				// 1.构建获取count的mappedStatement
//				SqlUtils.buildCountSql(sql);
//				countSqlSource = buildSqlSource(mappedStatement, boundSql,
//						sqlSource, args[1], sql, FLAG_COUNT, page, null,
//						resultMappingMap);
//				MappedStatement mappedStatementCount = buildMappedStatement(
//						mappedStatement, countSqlSource, FLAG_COUNT);
//				args[0] = mappedStatementCount;
//				// 查询总数
//				Object result = invocation.proceed();
				// 设置总数
				//Integer totalCount = (Integer) ((List) result).get(0);
				page.setTotalCount(totalCount);
			}

			if (!limitFlag) {
				sql = buildLimitForMysql(staticSql, page, FLAG_SEARCH);
				sqlSource = new StaticSqlSource(
						mappedStatement.getConfiguration(), sql,
						boundSql.getParameterMappings());
			}
			// 2.构建分页查询的mappedStatement
			SqlSource pageSqlSource = buildSqlSource(mappedStatement, boundSql,
					sqlSource, args[1], sql, FLAG_SEARCH, page, orders,
					resultMappingMap);
			MappedStatement mappedStatementPage = buildMappedStatement(
					mappedStatement, pageSqlSource, FLAG_SEARCH);
			args[0] = mappedStatementPage;

		}
		// 将执行权交给下一个拦截器
		return invocation.proceed();
	}

	private Map<String, String> converResultMappingList2Map(
			List<ResultMapping> resultMappings) {
		Map<String, String> resultMappingMap = new HashMap<String, String>();
		if (resultMappings != null && resultMappings.size() > 0) {
			for (ResultMapping resultMapping : resultMappings) {
				resultMappingMap.put(resultMapping.getProperty(),
						resultMapping.getColumn());
			}
		}
		return resultMappingMap;
	}

	@SuppressWarnings("unchecked")
	public SqlSource buildSqlSource(MappedStatement mappedStatement,
			BoundSql boundSql, SqlSource oSqlSource, Object parameterObject,
			String oldSql, String flag, Page page, List<Order> orders,
			Map<String, String> resultMappings) {
		SqlSource newSqlSource = null;

		if (oSqlSource instanceof DynamicSqlSource) {
			MetaObject msObject = MetaObject.forObject(mappedStatement,
					DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY);
			List<SqlNode> sqlNodes = (List<SqlNode>) msObject
					.getValue("sqlSource.rootSqlNode.contents");
			List<SqlNode> newSqlNodes = null;
			if (FLAG_SEARCH.equals(flag)) {
				newSqlNodes = buildSearchSqlNodes(sqlNodes, orders, page,
						resultMappings, oldSql);
			} else {
				newSqlNodes = buildCountSqlNodes(sqlNodes);
			}
			MixedSqlNode newMixedSqlNode = new MixedSqlNode(newSqlNodes);
			newSqlSource = new DynamicSqlSource(
					mappedStatement.getConfiguration(), newMixedSqlNode);
		} else {
			String newSql = null;
			if (FLAG_SEARCH.equals(flag)) {
				newSql = buildSearchSql(oldSql, orders, page, resultMappings);
			} else {
				newSql = countStartSql + oldSql + countEndSql;
			}
			newSqlSource = new StaticSqlSource(
					mappedStatement.getConfiguration(), newSql,
					boundSql.getParameterMappings());
		}
		return newSqlSource;
	}

	private List<SqlNode> buildSearchSqlNodes(List<SqlNode> sqlNodes,
			List<Order> orders, Page page, Map<String, String> resultMappings,
			String sql) {
		List<SqlNode> orderSqlNodes = new ArrayList<SqlNode>();
		List<SqlNode> newSqlNodes = new ArrayList<SqlNode>();
		orderSqlNodes.addAll(sqlNodes);
		// 排序
		if (orders != null && orders.size() > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append(" ORDER BY ");
			boolean flag = false;
			for (Order order : orders) {
				String columnName = resultMappings.get(order.getPropertyName());
				if (StringUtils.isBlank(columnName)) {
					String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_DB_ORDER_PROPERTY_ERROR;
					throw new FastDemoSystemException(errorCode,
							PropertyConfigurer.getErrorMessage(errorCode,
									order.getPropertyName()));
				}
				String dir = order.getDir();
				if (flag) {
					sb.append(",");
				}
				sb.append(columnName);
				sb.append(" ");
				sb.append(dir);
				flag = true;
			}
			orderSqlNodes.add(new TextSqlNode(sb.toString()));
		}
		// 分页
		newSqlNodes.addAll(orderSqlNodes);
		if (page != null) {
			Integer beginrow = (page.getCurrentPage() - 1) * page.getPageSize();
			Integer endrow = null;
			Integer customSize = page.getCustomSize();
			Integer pageSize = null;
			if (customSize != null && customSize > 0) {
				pageSize = customSize;
				endrow = beginrow + customSize;
			} else {
				pageSize = page.getPageSize();
				endrow = beginrow + pageSize;
			}
			if (DIALECT_MYSQL.equals(dialect) && limitFlag) {
				newSqlNodes = buildNewSqlNodesForMysql(newSqlNodes,
						String.valueOf(beginrow), String.valueOf(pageSize));
			} else if (DIALECT_ORACLE.equals(dialect)) {
				newSqlNodes = buildNewSqlNodesForOracle(newSqlNodes,
						String.valueOf(beginrow), String.valueOf(endrow));
			}
		}
		return newSqlNodes;
	}

	/**
	 * 对自定义limit进行分页控制
	 * 
	 * @param sql
	 * @param flag
	 * @param beginrow
	 * @param pageSize
	 */
	private String buildLimitForMysql(String sql, Page page, String flag) {
		if (FLAG_COUNT.equals(flag)) {
			sql = sql.replace("limit", "");
		} else {
			if (page != null) {
				Integer beginrow = (page.getCurrentPage() - 1)
						* page.getPageSize();
				Integer customSize = page.getCustomSize();
				Integer pageSize = null;
				if (customSize != null && customSize > 0) {
					pageSize = customSize;
				} else {
					pageSize = page.getPageSize();
				}
				sql = sql.replace("limit", "limit " + beginrow + "," + pageSize
						+ " ");
			}
		}

		return sql;
	}

	public List<SqlNode> buildCountSqlNodes(List<SqlNode> oldSqlNodes) {
		List<SqlNode> newSqlNodes = new ArrayList<SqlNode>();
		newSqlNodes.add(countStartSqlNode);
		newSqlNodes.addAll(oldSqlNodes);
		newSqlNodes.add(countEndSqlNode);
		return newSqlNodes;
	}

	public List<SqlNode> buildNewSqlNodesForMysql(List<SqlNode> newSqlNodes,
			String beginrow, String pageSize) {
		TextSqlNode pageEndSqlNode = new TextSqlNode(" limit "
				+ beginrow + "," + pageSize);
		newSqlNodes.add(pageEndSqlNode);
		return newSqlNodes;
	}

	public List<SqlNode> buildNewSqlNodesForOracle(List<SqlNode> newSqlNodes,
			String beginrow, String endrow) {
		List<SqlNode> resultSqlNodes = new ArrayList<SqlNode>();
		resultSqlNodes.add(pageStartSqlNode);
		resultSqlNodes.addAll(newSqlNodes);
		TextSqlNode pageEndSqlNode = new TextSqlNode(pageMidSql
				+ beginrow + pageEndSql + endrow);
		resultSqlNodes.add(pageEndSqlNode);
		return resultSqlNodes;
	}

	private String buildSearchSql(String oldSql, List<Order> orders, Page page,
			Map<String, String> resultMappings) {
		// 排序
		StringBuilder sb = new StringBuilder();
		sb.append(oldSql);
		if (orders != null && orders.size() > 0) {
			sb.append(" ORDER BY ");
			boolean flag = false;
			for (Order order : orders) {
				String columnName = resultMappings.get(order.getPropertyName());
				if (StringUtils.isBlank(columnName)) {
					String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_DB_ORDER_PROPERTY_ERROR;
					throw new FastDemoSystemException(errorCode,
							PropertyConfigurer.getErrorMessage(errorCode,
									order.getPropertyName()));
				}
				String dir = order.getDir();
				if (flag) {
					sb.append(",");
				}
				sb.append(columnName);
				sb.append(" ");
				sb.append(dir);
				flag = true;
			}
		}
		// 分页
		if (page != null) {
			String beginrow = String.valueOf((page.getCurrentPage() - 1)
					* page.getPageSize());
			String endrow = String.valueOf(page.getCurrentPage()
					* page.getPageSize());
			String pageSize = String.valueOf(page.getPageSize());

			if (DIALECT_MYSQL.equals(dialect) && limitFlag) {
				sb.append(" limit ");
				sb.append(beginrow);
				sb.append(",");
				sb.append(pageSize);
			} else if (DIALECT_ORACLE.equals(dialect)) {
				String midSql = sb.toString();
				sb = new StringBuilder();
				sb.append(pageStartSql);
				sb.append(midSql);
				sb.append(pageMidSql);
				sb.append(beginrow);
				sb.append(pageEndSql);
				sb.append(endrow);
			}
		}
		return sb.toString();
	}

	/**
	 * 新建count查询和分页查询的MappedStatement
	 *
	 * @param ms
	 * @param sqlSource
	 * @param suffix
	 * @return
	 */
	public MappedStatement buildMappedStatement(MappedStatement ms,
			SqlSource sqlSource, String flag) {
		String id = ms.getId();
		MappedStatement.Builder builder = new MappedStatement.Builder(
				ms.getConfiguration(), id, sqlSource, ms.getSqlCommandType());
		builder.resource(ms.getResource());
		builder.fetchSize(ms.getFetchSize());
		builder.statementType(ms.getStatementType());
		builder.keyGenerator(ms.getKeyGenerator());
		if (ms.getKeyProperties() != null && ms.getKeyProperties().length != 0) {
			StringBuilder keyProperties = new StringBuilder();
			for (String keyProperty : ms.getKeyProperties()) {
				keyProperties.append(keyProperty).append(",");
			}
			keyProperties.delete(keyProperties.length() - 1,
					keyProperties.length());
			builder.keyProperty(keyProperties.toString());
		}
		builder.timeout(ms.getTimeout());
		builder.parameterMap(ms.getParameterMap());
		if (FLAG_SEARCH.equals(flag)) {
			builder.resultMaps(ms.getResultMaps());
		} else {
			// count查询返回值int
			List<ResultMap> resultMaps = new ArrayList<ResultMap>();
			ResultMap resultMap = new ResultMap.Builder(ms.getConfiguration(),
					id, int.class, EMPTY_RESULTMAPPING).build();
			resultMaps.add(resultMap);
			builder.resultMaps(resultMaps);
		}
		builder.resultSetType(ms.getResultSetType());
		builder.cache(ms.getCache());
		builder.flushCacheRequired(ms.isFlushCacheRequired());
		builder.useCache(ms.isUseCache());

		return builder.build();
	}

	private boolean isMatch(String methodName) {
		if (methodNames == null || methodNames.size() <= 0) {
			return false;
		}
		for (String mtName : methodNames) {
			if (MatchUtils.match(mtName, methodName)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 只拦截Executor
	 *
	 * @param target
	 * @return
	 */
	public Object plugin(Object target) {
		if (target instanceof Executor) {
			return Plugin.wrap(target, this);
		} else {
			return target;
		}
	}

	public void setProperties(Properties propers) {
	}

	public String getDialect() {
		return dialect;
	}

	public List<String> getMethodNames() {
		return methodNames;
	}

	public void setMethodNames(List<String> methodNames) {
		this.methodNames = methodNames;
	}

	public void setDialect(String dialect) {
		if (StringUtils.isBlank(dialect)) {
			this.dialect = this.defaultDialect;
		} else {
			this.dialect = dialect;
		}
	}

	/**
	 * 复制BoundSql对象
	 */
	private BoundSql copyFromBoundSql(MappedStatement ms, BoundSql boundSql,
			String sql) {
		BoundSql newBoundSql = new BoundSql(ms.getConfiguration(), sql,
				boundSql.getParameterMappings(), boundSql.getParameterObject());
		for (ParameterMapping mapping : boundSql.getParameterMappings()) {
			String prop = mapping.getProperty();
			if (boundSql.hasAdditionalParameter(prop)) {
				newBoundSql.setAdditionalParameter(prop,
						boundSql.getAdditionalParameter(prop));
			}
		}
		return newBoundSql;
	}

	private static class NullObject {
	}
}