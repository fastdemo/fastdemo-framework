package com.fastdemo.framework.constant;

/**
 * 删除标记常量
 */
public class IsDeleteConstants {

	/**
	 * 删除
	 */
	public static final Integer DELETE_ON = 1;
	
	/**
	 * 未删除
	 */
	public static final Integer DELETE_OFF = 0;
	
}
