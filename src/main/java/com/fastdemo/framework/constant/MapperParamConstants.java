package com.fastdemo.framework.constant;

public class MapperParamConstants {

	public static final String PARAM_QUERY = "query";

	public static final String PARAM_DATA = "data";

	public static final String PARAM_IDS = "ids";

	public static final String PROPERTY_ID = "propertyId";

	public static final String PROPERTY_VALUES = "propertyValues";

}
