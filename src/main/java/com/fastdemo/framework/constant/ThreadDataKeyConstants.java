package com.fastdemo.framework.constant;

public class ThreadDataKeyConstants {

	public static final String THREAD_DATA_KEY_USER_OBJECT = "ThreadLocalUser";
	
	public static final String THREAD_DATA_KEY_DECODE_KEY = "decodeKey";

	public static final String THREAD_DATA_KEY_ENCODE_KEY = "encodeKey";
	
	public static final String THREAD_DATA_KEY_USER_INFOR_MAP = "userInforMap";
	
	public static final String THREAD_DATA_KEY_COMPANY_INFOR_MAP = "companyInforMap";
	
	public static final String THREAD_DATA_KEY_IS_DECRYPTION = "security";
	
	public static final String THREAD_DATA_KEY_CELL_LOG = "cellLog";
	
	public static final String THREAD_DATA_KEY_CELL_LOG_REQUEST_BODY = "requestBody";
	
	public static final String THREAD_DATA_KEY_CELL_LOG_RESPONSE_BODY = "responseBody";
	
	public static final String THREAD_DATA_KEY_DATA_SOURCE_NAME = "dataSourceName";
	
}
