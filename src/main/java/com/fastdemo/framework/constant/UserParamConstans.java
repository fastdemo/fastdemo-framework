package com.fastdemo.framework.constant;

/**
 * Thread Data 中用户的属性key
 *
 */
public class UserParamConstans {

	/**
	 * 用户主键id缓存key
	 */
	public static final String KEY_USER_ID = "id";
	
	/**
	 * 用户登录id缓存key
	 */
	public static final String KEY_USER_USERNO = "userNo";
	
	/**
	 * 公司code
	 */
	public static final String KEY_USER_COMPANY_CODE = "companyCode";
	
	/**
	 * 用户名缓存key
	 */
	public static final String KEY_USER_NAME = "userName";
	
	/**
	 * 用户手机号缓存key
	 */
	public static final String KEY_USER_PHONE_NO = "phoneNo";
	
	/**
	 * 用户邮箱缓存key
	 */
	public static final String KEY_USER_EMAIL = "email";
	
	/**
	 * 用户userType缓存key
	 */
	public static final String KEY_USER_TYPE = "userType";

	/**
	 * 用户所在部门缓存key
	 */
	public static final String KEY_USER_ACTUAL_DEPT = "actualDepartment";

	/**
	 * 部门主键id缓存key
	 */
	public static final String KEY_DEPT_ID = "id";

	/**
	 * 部门代码缓存key
	 */
	public static final String KEY_DEPT_CODE = "departCode";

	/**
	 * 部门名称缓存key
	 */
	public static final String KEY_DEPT_NAME = "departName";
	
	
	/**
	 * 用户密码缓存key
	 */
	public static final String KEY_USER_PASS = "userPass";
	
	/**
	 * RL用户缓存key
	 */
	public static final String KEY_RL_USE_TYPE = "useType";
	
	/**
	 * PL用户服务器IP缓存key
	 */
	public static final String KEY_PL_SERVER_IP = "serverIp";
	
	/**
	 * 用户系统状态缓存key
	 */
	public static final String KEY_USER_SYSTEM_STATUS = "systemStatus";
	
	/**
	 * 用户免打扰缓存key
	 */
	public static final String KEY_USER_BUSINESS_STATUS = "businessStatus";
	
	/**
	 * 用户token缓存key
	 */
	public static final String KEY_USER_TOKEN = "userToken";
	
	/**
	 * 用户加密key缓存key
	 */
	public static final String KEY_USER_ENCODE_KEY = "encodeKey";
	
	/**
	 * 用户设备id缓存key
	 */
	public static final String KEY_USER_DEVICE_ID = "deviceId";
	
	/**
	 * 用户设备id缓存key
	 */
	public static final String KEY_USER_DEVICE_SYS_TYPE = "deviceSysType";
	
	/**
	 * 用户所在子系统代码名缓存key
	 */
	public static final String KEY_ADMIN_USER_SYSTEM_CODE = "systemCode";
	
	/**
	 * 用户角色缓存key
	 */
	public static final String KEY_ADMIN_USER_ROLES = "roles";
	
	/**
	 * 角色id缓存key
	 */
	public static final String KEY_ADMIN_ROLE_ID = "id";
	
	/**
	 * 角色代码缓存key
	 */
	public static final String KEY_ADMIN_ROLE_CODE = "roleCode";
	
	/**
	 * 角色描述缓存key
	 */
	public static final String KEY_ADMIN_ROLE_DES = "roleDes";
	
	/**
	 * 角色名缓存key
	 */
	public static final String KEY_ADMIN_ROLE_NAME = "roleName";
	
	/**
	 * 角色所在系统代码缓存key
	 */
	public static final String KEY_ADMIN_ROLE_SYSTEM_CODE = "roleSystemCode";
	
	/**
	 * 角色资源缓存key
	 */
	public static final String KEY_ADMIN_ROLE_RESOURCE = "resources";
	
	/**
	 * 资源id缓存key
	 */
	public static final String KEY_ADMIN_RESOURCE_ID = "id";
	
	/**
	 * 角色资源缓存key
	 */
	public static final String KEY_ADMIN_RESOURCE_CODE = "resCode";
	
	/**
	 * 角色资源缓存key
	 */
	public static final String KEY_ADMIN_RESOURCE_NAME = "resName";
	
	/**
	 * 角色资源缓存key
	 */
	public static final String KEY_ADMIN_RESOURCE_PARENT_ID = "parentId";
	
	/**
	 * 角色资源缓存key
	 */
	public static final String KEY_ADMIN_RESOURCE_URL = "resUrl";
	
	/**
	 * 角色资源缓存key
	 */
	public static final String KEY_ADMIN_RESOURCE_SYSTEN_CODE = "resSystemCode";
	
	/**
	 * 资源URL缓存key
	 */
	public static final String KEY_ADMIN_RESOURCE_URL_CODE = "resURL";
	
	/**
	 * 设备用户主键id缓存key
	 */
	public static final String KEY_DEVICE_USER_ID = "userId";
	
	/**
	 * 用户真实姓名缓存key
	 */
	public static final String KEY_USER_REAL_NAME = "userRealName";
	
	/**
	 * 用户手机缓存key
	 */
	public static final String KEY_USER_MOBILE = "mobile";
	
	/**
	 * 用户生日缓存key
	 */
	public static final String KEY_USER_BIRTHDAY = "userBirthday";
	
	/**
	 * 用户生日缓存key
	 */
	public static final String KEY_USER_SEX = "userSex";
	
	/**
	 * 用户证件类型缓存key
	 */
	public static final String KEY_USER_IDENTIT_TYPE = "userIdentityType";
	
	/**
	 * 用户证件号缓存key
	 */
	public static final String KEY_USER_IDENTIT_NO = "userIdentityNo";
	
	/**
	 * 用户证件正面图片路径缓存key
	 */
	public static final String KEY_USER_IDENTIT_FRONT_IMG_URL = "userIdentityFrontImgUrl";
	
	/**
	 * 用户证件反面图片路径缓存key
	 */
	public static final String KEY_USER_IDENTIT_BACK_IMG_URL = "userIdentityBackImgUrl";
	
	/**
	 * 用户法人证件图片路径缓存key
	 */
	public static final String KEY_USER_LEGAL_PERSON_CERTIFICATE_IMG_URL= "legalPersonCertificateImgUrl";
	
	/**
	 * 企业id缓存key
	 */
	public static final String KEY_COMPANY_ID= "id";
	
	public static final String SUPER_ADMIN = "superadmin";
	
	public static final String DEPARTMENT = "department";

	public static final String ACTUAL_DEPARTMENT = "actualDepartment";

	public static final String OUTOFSESSION = "outOfSession";
	
	public static final Integer ONLINE_OK = 0;
	
	public static final Integer ONLINE_OUT = 1;
	
	
}
