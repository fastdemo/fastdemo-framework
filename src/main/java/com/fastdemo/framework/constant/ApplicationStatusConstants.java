package com.fastdemo.framework.constant;



public class ApplicationStatusConstants {
	
	//BindException 正常的业务异常
	
	//EliangException 正常的业务异常
	
	//MethodArgumentNotValidException 正常的业务异常
	
	/**
	 * 消息体为空
	 */
	public static final Integer APP_STATUS_EXCEPTION_HTTP_MESSAGE_NOT_READABLE = 1;
	
	/**
	 * 上传文件异常
	 */
	public static final Integer APP_STATUS_EXCEPTION_MULTIPART = 2;
	
	/**
	 * 上传文件大小超限异常
	 */
	public static final Integer APP_STATUS_EXCEPTION_MAX_UPLOAD_SIZE_EXCEEDED = 3;
	
	/**
	 * json转换异常
	 */
	public static final Integer APP_STATUS_EXCEPTION_JSON_PROCESSING = 4;
	
	/**
	 * 消息体转换异常
	 */
	public static final Integer APP_STATUS_EXCEPTION_HTTP_MESSAGE_COVERSION = 5;
	
	/**
	 * 其他未知异常
	 */
	public static final Integer APP_STATUS_EXCEPTION_UN_KNOW = 6;
	
}
