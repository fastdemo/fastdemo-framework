package com.fastdemo.framework.constant;

public class SystemConstants {
	
	/**
	 * app user agent
	 */
	public static final String APP_USER_AGENT = "app.userAgent";
	
	/**
	 * 系统id
	 */
	public static final String APP_ID = "app.id";
	
	/**
	 * 系统id
	 */
	public static final String APP_VERSION = "app.version";
	
	/**
	 * 系统运行模式
	 */
//	public static final String APP_MODE = "app.mode";
	
	/**
	 * 用户缓存前缀
	 */
	public static final String APP_USER_PREFIX = "app.user.prefix";
	
	/**
	 * 系统error code
	 */
	public static final String APP_ERROR_CODE = "app.errorCode";
	
	/**
	 * 导出数据分页大小
	 */
	public static final String EXPORT_PAGE_SIZE = "export.page.size";
	
	/**
	 * 导出xls行缓存
	 */
	public static final String EXPORT_EXCEL_CACHE = "export.excel.cache";
	
	/**
	 * 导出xls最大行数
	 */
	public static final String EXPORT_EXCEL_MAX_ROWS = "export.excel.max.rows";
	
	/**
	 * 技术日志开关
	 */
	public static final String FLAG_LOG_CELL = "flag.log.cell";
	
	/**
	 * HTTP机密解密开关
	 */
	public static final String FLAG_DECRYTION = "flag.http.decryption";
	
	/**
	 * 导入批次数量限制
	 */
	public static final String IMPORT_BATCH_SIZE = "import.batch.size";
	
	/**
	 * message 保存开关
	 */
	public static final String FLAG_MESSAGE_INFO = "flag.message.info";
	
	/**
	 * message error 保存开关
	 */
	public static final String FLAG_MESSAGE_ERROR = "flag.message.error";
	
	/**
	 * 系统默认message info collection
	 */
	public static final String COLLECTION_MESSAGE_INFO = "collection.message.info";
	
	/**
	 * 系统默认message error collection
	 */
	public static final String COLLECTION_MESSAGE_ERROR = "collection.message.error";
	
	/**
	 * 系统扫描controller包路径
	 */
	public static final String SCAN_PACKAGE_CONTROLLER = "scan.package.controller";

}
