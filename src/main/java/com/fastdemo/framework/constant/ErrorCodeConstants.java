package com.fastdemo.framework.constant;

public class ErrorCodeConstants {
	
	
	public static final Integer ERRORCODE_TYPE_SYSTEM = 0;
	
	public static final Integer ERRORCODE_TYPE_LOCAL = 1;
	
	public static final Integer ERRORCODE_TYPE_REMOTE = 2;
	
	public static final Integer ERRORCODE_TYPE_BIZ = 3;
	
	
	public static final String FASTDEMO = "E0000";

	public static final String FRAMEWORK = "0";

	public static final String HTTP = "00";

	public static final String DB = "01";
	
	public static final String UTIL = "02";
	
	public static final String FILE = "03";
	
	public static final String MQ = "04";
	
	public static final String THREAD_DATA = "05";
	
	public static final String SEARCH = "06";
	
	public static final String DICTIONARY = "07";
	
	public static final String INIT = "08";
	
	public static final String PROPERTY = "09";
	
	public static final String TASK = "10";
	
	
	
	
	
	/**
	 * property error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_PROPERTY_NOT_EXIST = FASTDEMO
			+ FRAMEWORK + PROPERTY + "00";
	
	/**
	 * property wran message
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_TASK_ERROR = FASTDEMO
			+ FRAMEWORK + TASK + "00";
	
	/**
	 * property blank
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_PROPERTY_BLANK = FASTDEMO
			+ FRAMEWORK + PROPERTY + "01";
	
	/**
	 * property wran message
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_PROPERTY_WRAN = FASTDEMO
			+ FRAMEWORK + PROPERTY + "02";
	
	
	
	/**
	 * HTTP OK 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_SUCCESS = FASTDEMO + FRAMEWORK
			+ HTTP + "00";
	
	/**
	 * HTTP Exception
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_EXCEPTION = FASTDEMO
			+ FRAMEWORK + HTTP + "01";
	
	/**
	 * Valid Exception
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_VALID_EXCEPTION = FASTDEMO
			+ FRAMEWORK + HTTP + "02";
	
	/**
	 * Json Exception
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_JSON_EXCEPTION = FASTDEMO
			+ FRAMEWORK + HTTP + "03";
	
	/**
	 * Message Convert Exception
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_MESSAGE_CONVERT_EXCEPTION = FASTDEMO
			+ FRAMEWORK + HTTP + "04";
	
	/**
	 * Business Error Exception
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_BUSINESS_ERROR_EXCEPTION = FASTDEMO
			+ FRAMEWORK + HTTP + "05";
	
	/**
	 * login need
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_LOGIN_NEED = FASTDEMO
			+ FRAMEWORK + HTTP + "10";
	
	/**
	 * login password error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_LOGIN_PASSWORD_ERROR = FASTDEMO
			+ FRAMEWORK + HTTP + "11";
	
	/**
	 * login unknown account
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_LOGIN_UNKNOWN_ACCOUNT = FASTDEMO
			+ FRAMEWORK + HTTP + "12";
	
	/**
	 * login locked account
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_LOGIN_LOCKED_ACCOUNT = FASTDEMO
			+ FRAMEWORK + HTTP + "13";
	
	/**
	 * login unknown error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_LOGIN_UNKNOWN_ERROR = FASTDEMO
			+ FRAMEWORK + HTTP + "14";
	
	/**
	 * logout unknown error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_LOGOUT_UNKNOWN_ERROR = FASTDEMO
			+ FRAMEWORK + HTTP + "15";
	
	/**
	 * Resource not allow Exception
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_RESOURCE_NOT_ALLOW = FASTDEMO
			+ FRAMEWORK + HTTP + "16";
	
	/**
	 * security not dfefine Exception
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_SECURITY_NOT_DFEFINE = FASTDEMO
			+ FRAMEWORK + HTTP + "17";
	
	/**
	 * Http body not exist
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_BODY_ERROR = FASTDEMO
			+ FRAMEWORK + HTTP + "18";
	
	/**
	 * Http upload file error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_UPLOAD_FILE_ERROR = FASTDEMO
			+ FRAMEWORK + HTTP + "19";
	
	/**
	 * Http upload file exceed
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_UPLOAD_FILE_EXCEED = FASTDEMO
			+ FRAMEWORK + HTTP + "20";
	
	/**
	 * Http order property error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_ORDER_PROPERTY_ERROR = FASTDEMO
			+ FRAMEWORK + HTTP + "21";
	
	/**
	 * Http permission error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_PERMISSION_ERROR = FASTDEMO
			+ FRAMEWORK + HTTP + "22";
	
	/**
	 * Http valid error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_DATE_VALID_ERROR = FASTDEMO
			+ FRAMEWORK + HTTP + "23";
	
	/**
	 * login need
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_OTHER_PLACE_LOGIN_ERROR = FASTDEMO
			+ FRAMEWORK + HTTP + "24";
	
	/**
	 * file type not support
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_FILE_TYPE_NOT_SUPPORT = FASTDEMO
			+ FRAMEWORK + HTTP + "25";
	
	/**
	 * file size over flow
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_FILE_SIZE_OVERFLOW = FASTDEMO
			+ FRAMEWORK + HTTP + "26";
	
	/**
	 * file size over flow
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_HTTP_JSON_DATE_FORMAT = FASTDEMO
			+ FRAMEWORK + HTTP + "27";

	/**
	 * Thread Data User not exist
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_THREAD_DATA_USER_NOT_EXIST = FASTDEMO
			+ FRAMEWORK + THREAD_DATA + "01";
	
	/**
	 * Thread Data User not exist
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_THREAD_DATA_USER_ID_NOT_EXIST = FASTDEMO
			+ FRAMEWORK + THREAD_DATA + "02";
	
	/**
	 * Thread Data Company not exist
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_THREAD_DATA_COMPANY_NOT_EXIST = FASTDEMO
			+ FRAMEWORK + THREAD_DATA + "03";
	
	/**
	 * Thread Data Company Code not exist
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_THREAD_DATA_COMPANY_CODE_NOT_EXIST = FASTDEMO
			+ FRAMEWORK + THREAD_DATA + "04";
	
	/**
	 * UTIL Convert Entity List to DTO List Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_ENTITYLIST_TO_DTOLIST_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "00";
	/**
	 * UTIL Convert Map to DTO Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_MAP_TO_DTO_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "01";
	
	/**
	 * UTIL Convert Map to Entity Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_MAP_TO_ENTITY_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "02";
	
	/**
	 * UTIL Convert PageDTO to Page Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_PAGEDTO_TO_PAGE_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "03";
	
	/**
	 * UTIL Convert Entity to DTO Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_ENTITY_TO_DTO_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "04";
	
	/**
	 * UTIL Convert JSON to BEAN Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_JSON_TO_BEAN_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "05";
	
	/**
	 * UTIL Convert BEAN to JSON Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_BEAN_TO_JSON_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "06";
	
	/**
	 * UTIL Get Field Value Error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_GET_FIELD_VALUE_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "07";
	
	/**
	 * UTIL Set Field Value Error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_SET_FIELD_VALUE_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "08";
	
	/**
	 * UTIL Field Can not be null
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_FIELD_NULL_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "09";
	
	/**
	 * UTIL Excel Import Error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_EXCEL_IMPORT_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "10";
	
	/**
	 * UTIL Convert DTO to Entity Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_DTO_TO_ENTITY_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "11";
	
	/**
	 * Build Url Normal Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_BUILD_URL_NORMAL_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "12";
	
	/**
	 * Build Url Error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_BUILD_URL_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "13";
	
	/**
	 * UTIL Convert Support to DTO Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_DTO_TO_SUPPORT_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "14";
	
	/**
	 * UTIL Convert Support List to DTO List Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_DTOLIST_TO_SUPPORTLIST_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "15";
	
	/**
	 * UTIL Convert Support to DTO Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_SUPPORT_TO_DTO_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "16";
	
	/**
	 * UTIL Convert Support List to DTO List Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_SUPPORTLIST_TO_DTOLIST_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "17";
	
	/**
	 * UTIL Convert DTO List to Entity List Error 
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_UTIL_DTOLIST_TO_ENTITYLIST_ERROR = FASTDEMO
			+ FRAMEWORK + UTIL + "18";
	
	/**
	 * MQ Json Exception
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_MQ_JSON_EXCEPTION = FASTDEMO
			+ FRAMEWORK + MQ + "01";
	
	/**
	 * MQ Json Exception
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_MQ_UNKNOWN_EXCEPTION = FASTDEMO
			+ FRAMEWORK + MQ + "02";
	
	/**
	 * MQ Failed to convert Message content
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_MQ_MESSAGE_CONVERT_FAILED = FASTDEMO
			+ FRAMEWORK + MQ + "03";
	
	/**
	 * MQ Original message save error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_MQ_ORIGINAL_MESSAGE_SAVE_ERROR = FASTDEMO
			+ FRAMEWORK + MQ + "04";
	
	/**
	 * DB Result not singleton
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_DB_RESULT_NOT_SINGLETON = FASTDEMO
			+ FRAMEWORK + DB + "00";
	
	/**
	 * DB Record not exist
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_DB_RECORD_NOT_EXIST = FASTDEMO
			+ FRAMEWORK + DB + "01";
	
	/**
	 * Mongo delete error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_MONGO_DELETE_ERROR = FASTDEMO
			+ FRAMEWORK + DB + "02";
	
	/**
	 * db logical delete error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_DB_LOGICAL_DELETE_ERROR = FASTDEMO
			+ FRAMEWORK + DB + "03";
	
	/**
	 * DB Record not exist
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_DB_RECORDS_NOT_EXIST = FASTDEMO
			+ FRAMEWORK + DB + "04";
	
	/**
	 * db delete error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_DB_DELETE_ERROR = FASTDEMO
			+ FRAMEWORK + DB + "05";
	
	/**
	 * db order propery is not exist
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_DB_ORDER_PROPERTY_ERROR = FASTDEMO
			+ FRAMEWORK + DB + "06";
	
	/**
	 * batch update error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_DB_BATCH_UPDATE_ERROR = FASTDEMO
			+ FRAMEWORK + DB + "07";
	
	/**
	 * batch update error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_DB_DYNAMIC_DATA_SOURCE_REEOR = FASTDEMO
			+ FRAMEWORK + DB + "08";
	
	/**
	 * batch update error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_DB_DYNAMIC_DATA_SOURCE_NOT_FOUND_REEOR = FASTDEMO
			+ FRAMEWORK + DB + "09";
	
	/**
	 * batch update error
	 */
	public static final String MSG_FASTDEMO_FRAMEWORK_DB_DYNAMIC_DATA_SOURCE_EXIST_REEOR = FASTDEMO
			+ FRAMEWORK + DB + "10";
	
}
