package com.fastdemo.framework.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import com.fastdemo.framework.entity.BaseEntity;
import com.fastdemo.framework.entity.Order;
import com.fastdemo.framework.entity.Page;
import com.fastdemo.framework.entity.Property;
import com.fastdemo.framework.entity.Search;

public class EntityUtils {

	/**
	 * AbstractEntityService log
	 */
	private static final Logger logger = LoggerFactory
			.getLogger(EntityUtils.class);

	public static final String PROPERTY_NAME_CREATE_TIME = "createTime";

	public static <T extends BaseEntity> Map<String, Object> buildSelectMap(
			T t, boolean notNull) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (t == null) {
			return map;
		}
		List<Field> fields = ReflectUtils.getFields(t, Object.class);
		for (Field field : fields) {
			if (Modifier.isStatic(field.getModifiers())) {
				continue;
			}
			Object value = ReflectUtils.getFieldValue(t, field);
			if (value == null && notNull) {
				continue;
			}
			if (value instanceof String) {
				String sValue = (String) value;
				if (StringUtils.isBlank(sValue) && notNull) {
					continue;
				}
			}
			String fieldName = field.getName();
			map.put(fieldName, value);
		}
		return map;
	}

	public static <T> Map<String, Object> buildSelectMap(Search<?> search,
			boolean notNull) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(Page.PAGE_NAME, search.getPage());
		List<Order> orders = search.getOrders();
		if (orders == null) {
			orders = new ArrayList<Order>();
		}
		if (orders.size() <= 0) {
			Order order = new Order();
			order.setDir(Order.ORDER_DESC);
			order.setPropertyName(PROPERTY_NAME_CREATE_TIME);
			orders.add(order);
		}
		map.put(Order.ORDER_NAME, orders);
		Object entity = search.getQueryParams();
		if (entity != null) {
			List<Field> fields = ReflectUtils.getFields(entity, Object.class);
			for (Field field : fields) {
				if (Modifier.isStatic(field.getModifiers())) {
					continue;
				}
				Object value = ReflectUtils.getFieldValue(entity, field);
				if (value == null && notNull) {
					continue;
				}
				if (value instanceof String) {
					String sValue = (String) value;
					if (StringUtils.isBlank(sValue) && notNull) {
						continue;
					}
				}
				String fieldName = field.getName();
				map.put(fieldName, value);
			}
		}

		List<Property> properties = search.getProperties();
		if (properties != null && properties.size() > 0) {
			for (Property property : properties) {
				String propertyName = property.getPropertyName();
				Object oValue = map.get(propertyName);
				Object oPropertyValues = property.getPropertyValues();
				if (oValue != null) {
					logger.warn("query param key:" + propertyName + " value:"
							+ oValue + " override by:"
							+ JsonUtils.beanToJson(oPropertyValues));
				}
				map.put(propertyName, oPropertyValues);
			}
		}
		return map;
	}
	public static void copyProperties(Object source, Object target, String... ignoreProperties) throws BeansException {
		copyProperties(source, target, null, ignoreProperties);
	}
	
	public static void copyProperties(Object source, Object target, Class<?> editable, String... ignoreProperties)
			throws BeansException {

		Assert.notNull(source, "Source must not be null");
		Assert.notNull(target, "Target must not be null");

		Class<?> actualEditable = target.getClass();
		if (editable != null) {
			if (!editable.isInstance(target)) {
				throw new IllegalArgumentException("Target class [" + target.getClass().getName() +
						"] not assignable to Editable class [" + editable.getName() + "]");
			}
			actualEditable = editable;
		}
		PropertyDescriptor[] targetPds = BeanUtils.getPropertyDescriptors(actualEditable);
		List<String> ignoreList = (ignoreProperties != null ? Arrays.asList(ignoreProperties) : null);

		for (PropertyDescriptor targetPd : targetPds) {
			Method writeMethod = targetPd.getWriteMethod();
			if (writeMethod != null && (ignoreList == null || !ignoreList.contains(targetPd.getName()))) {
				PropertyDescriptor sourcePd = BeanUtils.getPropertyDescriptor(source.getClass(), targetPd.getName());
				if (sourcePd != null) {
					Method readMethod = sourcePd.getReadMethod();
					if (readMethod != null &&
							ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
						try {
							if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
								readMethod.setAccessible(true);
							}
							Object value = readMethod.invoke(source);
							if(value!=null){
								if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
									writeMethod.setAccessible(true);
								}
								writeMethod.invoke(target, value);
							}
						}
						catch (Throwable ex) {
							throw new FatalBeanException(
									"Could not copy property '" + targetPd.getName() + "' from source to target", ex);
						}
					}
				}
			}
		}
	}
}
