package com.fastdemo.framework.utils;

import java.util.HashMap;
import java.util.Map;

import com.fastdemo.framework.constant.UserParamConstans;
import com.fastdemo.framework.constant.ThreadDataKeyConstants;

/**
 */
public class ThreadDataUtils {

	/**
	 * 线程数据缓存
	 */
	private static ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<Map<String, Object>>();

	/**
	 * 根据Key从线程数据缓存中获取数据
	 * 
	 * @param dataKey
	 * @return
	 */
	public static void removeThreadData() {
		threadLocal.remove();
	}
	
	/**
	 * 根据Key从线程数据缓存中获取数据
	 * 
	 * @param dataKey
	 * @return
	 */
	public static void removeThreadData(String dataKey) {
		Map<String, Object> map = threadLocal.get();
		if (map != null) {
			map.remove(dataKey);
		}
	}
	
	/**
	 * 根据Key从线程数据缓存中获取数据
	 * 
	 * @param dataKey
	 * @return
	 */
	public static Object getThreadData(String dataKey) {
		Map<String, Object> map = threadLocal.get();
		if (map != null) {
			return map.get(dataKey);
		}
		return null;
	}

	/**
	 * 根据Key把数据保存到数据缓存中
	 * 
	 * @param dataKey
	 * @param data
	 */
	public static void setThreadData(String dataKey, Object data) {
		Map<String, Object> map = threadLocal.get();
		if (map == null) {
			map = new HashMap<String, Object>();
		}
		map.put(dataKey, data);
		threadLocal.set(map);
	}

	/**
	 * 根据Key从线程数据缓存中获取数据
	 * 
	 * @param dataKey
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getRealThreadData(String dataKey, Class<T> t) {
		Map<String, Object> map = threadLocal.get();
		if (map != null) {
			Object oData = map.get(dataKey);
			if(oData != null){
				return (T) map.get(dataKey);
			}
		}
		return null;
	}

	/**
	 * 根据Key从线程数据缓存中获取数据
	 * 
	 * @param dataKey
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> getMapThreadData(String dataKey) {
		Map<String, Object> map = threadLocal.get();
		if (map != null) {
			Object oData = map.get(dataKey);
			if(oData != null){
				return (Map<String, Object>) oData;
			}
		}
		return null;
	}

	public static String getUserId() {
		Map<String, Object> userInfoMap = ThreadDataUtils
				.getMapThreadData(ThreadDataKeyConstants.THREAD_DATA_KEY_USER_INFOR_MAP);
		if (userInfoMap == null) {
			return null;
		}
		Object oUserId = userInfoMap.get(UserParamConstans.KEY_USER_ID);
		if (oUserId == null) {
			return null;
		}
		return (String) oUserId;
	}
	
	public static String getUserName() {
		Map<String, Object> userInfoMap = ThreadDataUtils
				.getMapThreadData(ThreadDataKeyConstants.THREAD_DATA_KEY_USER_INFOR_MAP);
		if (userInfoMap == null) {
			return null;
		}
		Object oUserName = userInfoMap.get(UserParamConstans.KEY_USER_NAME);
		if (oUserName == null) {
			return null;
		}
		return (String) oUserName;
	}
	
	/**
	 * @Description 获取服务端登录用户的真实姓名
	 * @return
	 */
	public static String getUserRealName() {
		Map<String, Object> userInfoMap = ThreadDataUtils
				.getMapThreadData(ThreadDataKeyConstants.THREAD_DATA_KEY_USER_INFOR_MAP);
		if (userInfoMap == null) {
			return null;
		}
		Object oUserRealName = userInfoMap.get(UserParamConstans.KEY_USER_REAL_NAME);
		if (oUserRealName == null) {
			return null;
		}
		return (String) oUserRealName;
	}
	
	public static String getUserPhoneNo() {
		Map<String, Object> userInfoMap = ThreadDataUtils
				.getMapThreadData(ThreadDataKeyConstants.THREAD_DATA_KEY_USER_INFOR_MAP);
		if (userInfoMap == null) {
			return null;
		}
		Object phoneNo = userInfoMap.get(UserParamConstans.KEY_USER_PHONE_NO);
		if (phoneNo == null) {
			return null;
		}
		return (String) phoneNo;
	}
	
	public static Integer getUserBusinessStatus() {
		Map<String, Object> userInfoMap = ThreadDataUtils
				.getMapThreadData(ThreadDataKeyConstants.THREAD_DATA_KEY_USER_INFOR_MAP);
		if (userInfoMap == null) {
			return null;
		}
		Object businessStatus = userInfoMap.get(UserParamConstans.KEY_USER_BUSINESS_STATUS);
		if (businessStatus == null) {
			return null;
		}
		return (Integer) businessStatus;
	}
	
	public static Integer getUserSystemStatus() {
		Map<String, Object> userInfoMap = ThreadDataUtils
				.getMapThreadData(ThreadDataKeyConstants.THREAD_DATA_KEY_USER_INFOR_MAP);
		if (userInfoMap == null) {
			return null;
		}
		Object systemStatus = userInfoMap.get(UserParamConstans.KEY_USER_SYSTEM_STATUS);
		if (systemStatus == null) {
			return null;
		}
		return (Integer) systemStatus;
	}


	public static Map<String, Object> getUserMap() {
		Map<String, Object> userInfoMap = ThreadDataUtils
				.getMapThreadData(ThreadDataKeyConstants.THREAD_DATA_KEY_USER_INFOR_MAP);
		return userInfoMap;
	}

	/**
	 * @return
	 * @date 2018年9月4日
	 * @author guxingchun
	 */
	public static String getUserMobile() {
		Map<String, Object> userInfoMap = ThreadDataUtils
				.getMapThreadData(ThreadDataKeyConstants.THREAD_DATA_KEY_USER_INFOR_MAP);
		if (userInfoMap == null) {
			return null;
		}
		Object mobile = userInfoMap.get(UserParamConstans.KEY_USER_MOBILE);
		if (mobile == null) {
			return null;
		}
		return (String) mobile;
	}

}
