package com.fastdemo.framework.utils;

import com.fastdemo.framework.config.PropertyConfigurer;
import com.fastdemo.framework.constant.ErrorCodeConstants;
import com.fastdemo.framework.exception.business.FastDemoBusinessException;
import com.fastdemo.framework.exception.local.FastDemoLocalException;
import com.fastdemo.framework.exception.remote.FastDemoRemoteException;
import com.fastdemo.framework.exception.system.FastDemoSystemException;

public class ExceptionUtil {

	public static FastDemoBusinessException codeException(String errorCode, Integer errorType) {
		if(errorCode == null)
			return new FastDemoBusinessException(null);
		if(errorType == null)
			errorType = ErrorCodeConstants.ERRORCODE_TYPE_BIZ;
		
		if(ErrorCodeConstants.ERRORCODE_TYPE_BIZ.equals(errorType)) {
			throw new FastDemoBusinessException(errorCode, PropertyConfigurer.getErrorMessage(errorCode));
		} else if(ErrorCodeConstants.ERRORCODE_TYPE_SYSTEM.equals(errorType)) {
			throw new FastDemoSystemException(errorCode, PropertyConfigurer.getErrorMessage(errorCode));
		} else if(ErrorCodeConstants.ERRORCODE_TYPE_LOCAL.equals(errorType)) {
			throw new FastDemoLocalException(errorCode, PropertyConfigurer.getErrorMessage(errorCode));
		} else if(ErrorCodeConstants.ERRORCODE_TYPE_REMOTE.equals(errorType)) {
			throw new FastDemoRemoteException(errorCode, PropertyConfigurer.getErrorMessage(errorCode));
		} else
			throw new FastDemoBusinessException(errorCode, PropertyConfigurer.getErrorMessage(errorCode));
	}
	
	public static FastDemoBusinessException bizException(String errorCode) {
		throw codeException(errorCode, ErrorCodeConstants.ERRORCODE_TYPE_BIZ);
	}
	
	public static FastDemoBusinessException systemException(String errorCode) {
		throw codeException(errorCode, ErrorCodeConstants.ERRORCODE_TYPE_SYSTEM);
	}
	
	public static FastDemoBusinessException localException(String errorCode) {
		throw codeException(errorCode, ErrorCodeConstants.ERRORCODE_TYPE_LOCAL);
	}
	
	public static FastDemoBusinessException remoteException(String errorCode) {
		throw codeException(errorCode, ErrorCodeConstants.ERRORCODE_TYPE_REMOTE);
	}
}
