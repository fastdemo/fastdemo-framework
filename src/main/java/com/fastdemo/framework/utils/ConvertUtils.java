package com.fastdemo.framework.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.fastdemo.framework.config.PropertyConfigurer;
import com.fastdemo.framework.constant.ErrorCodeConstants;
import com.fastdemo.framework.dto.BaseDTO;
import com.fastdemo.framework.dto.OrderDTO;
import com.fastdemo.framework.dto.PageDTO;
import com.fastdemo.framework.dto.SearchDTO;
import com.fastdemo.framework.entity.BaseEntity;
import com.fastdemo.framework.entity.Order;
import com.fastdemo.framework.entity.Page;
import com.fastdemo.framework.entity.Search;
import com.fastdemo.framework.exception.system.FastDemoSystemException;

/**
 *
 */
public class ConvertUtils {

	private static Logger logger = LoggerFactory.getLogger(ConvertUtils.class);

	/**
	 * 转换searchDTO 到 search
	 * 
	 * @param searchDTO
	 * @param targetType
	 * @return
	 */
	public static <E extends BaseEntity, D extends BaseDTO> Search<E> convertSearchDTO2Search(
			SearchDTO<D> searchDTO, Class<E> targetType) {
		E entity = convertDTO2Entity(searchDTO.getEntityDTO(), targetType);
		Search<E> search = new Search<E>();
		Page page = convertPageDTO2Page(searchDTO.getPageDTO());
		List<Order> orders = convertOrderDTOList2OrderList(searchDTO
				.getOrderDTOs());
		if (entity == null) {
			try {
				entity = targetType.newInstance();
			} catch (Exception e) {
				String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_DTO_TO_ENTITY_ERROR;
				String errorMessage = PropertyConfigurer
						.getErrorMessage(errorCode);
				logger.error(errorMessage, e);
				throw new FastDemoSystemException(errorCode, errorMessage, e);
			}
		}
		search.setQueryParams(entity);
		search.setPage(page);
		search.setOrders(orders);
		return search;
	}
	
//	public static <S extends BaseSupport, D extends BaseDTO> SearchSupport<S> convertSearchDTO2SearchSupport(
//			SearchDTO<D> searchDTO, Class<S> targetType) {
//		S support = convertDTO2Support(searchDTO.getEntityDTO(), targetType);
//		SearchSupport<S> searchSupport = new SearchSupport<S>();
//		Page page = convertPageDTO2Page(searchDTO.getPageDTO());
//		List<Order> orders = convertOrderDTOList2OrderList(searchDTO
//				.getOrderDTOs());
//		if (support == null) {
//			try {
//				support = targetType.newInstance();
//			} catch (Exception e) {
//				String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_DTO_TO_SUPPORT_ERROR;
//				String errorMessage = PropertyConfigurer
//						.getErrorMessage(errorCode);
//				logger.error(errorMessage, e);
//				throw new PurangSystemException(errorCode, errorMessage, e);
//			}
//		}
//		searchSupport.setQueryParams(support);
//		searchSupport.setPage(page);
//		searchSupport.setOrders(orders);
//		return searchSupport;
//	}

	public static <E extends BaseEntity, D extends BaseDTO> SearchDTO<D> convertSearch2SearchDTO(
			Search<E> search, Class<D> targetType) {
		SearchDTO<D> searchDTO = new SearchDTO<D>();
		PageDTO pageDTO = convertPage2PageDTO(search.getPage());
		searchDTO.setPageDTO(pageDTO);
		List<D> records = convertEntityList2DTOList(search.getEntities(),
				targetType);
		searchDTO.setRecords(records);
		return searchDTO;
	}
	
//	public static <S extends BaseSupport, D extends BaseDTO> SearchDTO<D> convertSearchSupport2SearchDTO(
//			SearchSupport<S> searchSupport, Class<D> targetType) {
//		SearchDTO<D> searchDTO = new SearchDTO<D>();
//		PageDTO pageDTO = convertPage2PageDTO(searchSupport.getPage());
//		searchDTO.setPageDTO(pageDTO);
//		List<D> records = convertSupportList2DTOList(searchSupport.getSupports(),
//				targetType);
//		searchDTO.setRecords(records);
//		return searchDTO;
//	}

	public static <E extends BaseEntity, D extends BaseDTO> D convertEntity2DTO(
			E entity, Class<D> targetType) {
		if (entity == null) {
			return null;
		}
		D dins = null;
		try {
			dins = targetType.newInstance();
			BeanUtils.copyProperties(entity, dins);
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_ENTITY_TO_DTO_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
		return dins;
	}
	
//	public static <E extends BaseSupport, D extends BaseDTO> D convertSupport2DTO(
//			E support, Class<D> targetType) {
//		if (support == null) {
//			return null;
//		}
//		D dins = null;
//		try {
//			dins = targetType.newInstance();
//			BeanUtils.copyProperties(support, dins);
//		} catch (Exception e) {
//			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_SUPPORT_TO_DTO_ERROR;
//			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
//			logger.error(errorMessage, e);
//			throw new PurangSystemException(errorCode, errorMessage, e);
//		}
//		return dins;
//	}
	
//	public static <S extends BaseSupport, D extends BaseDTO> S convertDTO2Support(
//			D dto, Class<S> targetType) {
//		if (dto == null) {
//			return null;
//		}
//		S dins = null;
//		try {
//			dins = targetType.newInstance();
//			BeanUtils.copyProperties(dto, dins);
//		} catch (Exception e) {
//			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_SUPPORT_TO_DTO_ERROR;
//			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
//			logger.error(errorMessage, e);
//			throw new PurangSystemException(errorCode, errorMessage, e);
//		}
//		return dins;
//	}

	public static <E extends BaseEntity, D extends BaseDTO> E convertDTO2Entity(
			D dto, Class<E> targetType) throws FastDemoSystemException {
		if (dto == null) {
			return null;
		}
		E dins = null;
		try {
			dins = targetType.newInstance();
			BeanUtils.copyProperties(dto, dins);
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_DTO_TO_ENTITY_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
		return dins;
	}

	public static <E extends BaseEntity, D extends BaseDTO> List<E> convertDTOList2EntityList(
			List<D> dtos, Class<E> targetListType) {
		if(dtos == null){
			return null;
		}
		List<E> entities = new ArrayList<E>();
		try {
			for (D d : dtos) {
				E eins = targetListType.newInstance();
				BeanUtils.copyProperties(d, eins);
				entities.add(eins);
			}
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_DTOLIST_TO_ENTITYLIST_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
		return entities;
	}

	public static <E extends BaseEntity, D extends BaseDTO> List<D> convertEntityList2DTOList(
			List<E> entities, Class<D> targetListType) {
		List<D> DTOs = new ArrayList<D>();
		if (entities == null) {
			return DTOs;
		}
		try {
			for (E e : entities) {
				D dins = targetListType.newInstance();
				BeanUtils.copyProperties(e, dins);
				DTOs.add(dins);
			}
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_ENTITYLIST_TO_DTOLIST_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
		return DTOs;
	}
	
//	public static <E extends BaseSupport, D extends BaseDTO> List<D> convertSupportList2DTOList(
//			List<E> supports, Class<D> targetListType) {
//		List<D> DTOs = new ArrayList<D>();
//		if (supports == null) {
//			return DTOs;
//		}
//		try {
//			for (E e : supports) {
//				D dins = targetListType.newInstance();
//				BeanUtils.copyProperties(e, dins);
//				DTOs.add(dins);
//			}
//		} catch (Exception e) {
//			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_SUPPORTLIST_TO_DTOLIST_ERROR;
//			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
//			logger.error(errorMessage, e);
//			throw new PurangSystemException(errorCode, errorMessage, e);
//		}
//		return DTOs;
//	}
	
//	public static <S extends BaseSupport, D extends BaseDTO> List<S> convertDTOList2SupportList(
//			List<D> dtos, Class<S> targetListType) {
//		List<S> supports = new ArrayList<S>();
//		if (dtos == null) {
//			return supports;
//		}
//		try {
//			for (D e : dtos) {
//				S dins = targetListType.newInstance();
//				BeanUtils.copyProperties(e, dins);
//				supports.add(dins);
//			}
//		} catch (Exception e) {
//			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_SUPPORTLIST_TO_DTOLIST_ERROR;
//			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
//			logger.error(errorMessage, e);
//			throw new PurangSystemException(errorCode, errorMessage, e);
//		}
//		return supports;
//	}

	public static List<OrderDTO> convertOrderList2OrderDTOList(
			List<Order> orders) {
		List<OrderDTO> orderDTOs = null;
		if (orders != null && orders.size() > 0) {
			orderDTOs = new ArrayList<OrderDTO>();
			for (Order order : orders) {
				OrderDTO orderDTO = convertOrder2OrderDTO(order);
				orderDTOs.add(orderDTO);
			}
		}
		return orderDTOs;
	}

	public static List<Order> convertOrderDTOList2OrderList(
			List<OrderDTO> orderDTOs) {
		List<Order> orders = null;
		if (orderDTOs != null && orderDTOs.size() > 0) {
			orders = new ArrayList<Order>();
			for (OrderDTO orderDTO : orderDTOs) {
				Order order = convertOrderDTO2Order(orderDTO);
				orders.add(order);
			}
		}
		return orders;
	}

	public static Order convertOrderDTO2Order(OrderDTO orderDTO) {
		if (orderDTO == null) {
			return null;
		}
		Order order = new Order();
		order.setPropertyName(orderDTO.getPropertyName());
		// 校验order
		String dir = orderDTO.getDir();
		if (!Order.ORDER_DESC.equals(dir.toUpperCase()) && !Order.ORDER_ASC.equals(dir.toUpperCase())) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_ORDER_PROPERTY_ERROR;
			throw new FastDemoSystemException(errorCode,
					PropertyConfigurer.getErrorMessage(errorCode,
							orderDTO.getPropertyName(), dir));
		}
		order.setDir(orderDTO.getDir());
		return order;
	}

	public static OrderDTO convertOrder2OrderDTO(Order order) {
		if (order == null) {
			return null;
		}
		OrderDTO orderDTO = new OrderDTO();
		orderDTO.setPropertyName(order.getPropertyName());
		orderDTO.setDir(order.getDir());
		return orderDTO;
	}

	public static PageDTO convertPage2PageDTO(Page page) {
		if (page == null) {
			return null;
		}
		PageDTO pageDTO = new PageDTO();
		pageDTO.setPageNo(page.getCurrentPage());
		pageDTO.setPageCount(page.getPageCount());
		pageDTO.setPageSize(page.getPageSize());
		pageDTO.setTotalCount(page.getTotalCount());
		pageDTO.setCurrentSize(page.getCurrentSize());
		return pageDTO;
	}

	public static Page convertPageDTO2Page(PageDTO pageDTO) {
		if (pageDTO == null) {
			return null;
		}
		Page page = new Page();
		page.setCurrentPage(pageDTO.getPageNo());
		page.setPageSize(pageDTO.getPageSize());
		return page;
	}

	public static <D extends BaseDTO> void convertMap2DTO(
			Map<String, Object> map, D dto) {
		try {
			org.apache.commons.beanutils.BeanUtils.populate(dto, map);
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_MAP_TO_DTO_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
	}

	public static <E extends BaseEntity> void convertMap2Entity(
			Map<String, Object> map, E entity) {
		try {
			org.apache.commons.beanutils.BeanUtils.populate(entity, map);
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_MAP_TO_ENTITY_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
	}
	
//	public static <E extends BaseSupport> void convertMap2Support(
//			Map<String, Object> map, E spport) {
//		try {
//			org.apache.commons.beanutils.BeanUtils.populate(spport, map);
//		} catch (Exception e) {
//			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_MAP_TO_ENTITY_ERROR;
//			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
//			logger.error(errorMessage, e);
//			throw new PurangSystemException(errorCode, errorMessage, e);
//		}
//	}


}
