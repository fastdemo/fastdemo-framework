package com.fastdemo.framework.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fastdemo.framework.config.PropertyConfigurer;
import com.fastdemo.framework.constant.ErrorCodeConstants;
import com.fastdemo.framework.exception.system.FastDemoSystemException;

/**
 * 
 */
public class JsonUtils {

	private static Logger logger = LoggerFactory.getLogger(JsonUtils.class);

	/**
	 * 
	 */
	private static ObjectMapper objectMapper = new ObjectMapper();

	static {
		objectMapper.setSerializationInclusion(Include.NON_NULL);
	}
	
	public ObjectMapper getMapper() {
		return objectMapper;
	}

	/**
	 * 将java对象转换成json字符串
	 * 
	 * @param obj
	 *            准备转换的对象
	 * @return json字符串
	 * @throws Exception
	 */
	public static String beanToJson(Object obj) {
		try {
			String json = objectMapper.writeValueAsString(obj);
			return json;
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_BEAN_TO_JSON_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
	}

	/**
	 * 将json字符串转换成java list对象
	 * 
	 * @param json
	 *            准备转换的json字符串
	 * @param cls
	 *            准备转换的类
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> jsonToList(String json, Class<T> elementClasses) {
		try {
			JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, elementClasses);
			return (List<T>) objectMapper.readValue(json, javaType);
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_JSON_TO_BEAN_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
	}

	/**
	 * 将json字符串转换成java map对象
	 * 
	 * @param json
	 *            准备转换的json字符串
	 * @param cls
	 *            准备转换的类
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> jsonToMap(String json, Class<K> keyClasses, Class<V> valueClasses) {
		try {
			JavaType javaType = objectMapper.getTypeFactory().constructParametricType(HashMap.class, keyClasses,
					valueClasses);
			return (Map<K, V>) objectMapper.readValue(json, javaType);
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_JSON_TO_BEAN_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
	}

	/**
	 * 将java对象转换成json字符串
	 * 
	 * @param obj
	 *            准备转换的对象
	 * @return json字符串
	 * @throws Exception
	 */
	public static String beanToJson(Object obj, boolean notNull) {
		try {
			String json = objectMapper.writeValueAsString(obj);
			return json;
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_BEAN_TO_JSON_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
	}

	/**
	 * 将json字符串转换成java对象
	 * 
	 * @param json
	 *            准备转换的json字符串
	 * @param cls
	 *            准备转换的类
	 * @return
	 * @throws Exception
	 */
	public static Object jsonToBean(String json, Class<?> cls) {
		try {
			Object bean = objectMapper.readValue(json, cls);
			return bean;
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_JSON_TO_BEAN_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
	}

	/**
	 * 将json字节数组转换成java对象
	 * 
	 * @param json
	 *            准备转换的json字符串
	 * @param cls
	 *            准备转换的类
	 * @return
	 * @throws Exception
	 */
	public static Object jsonToBean(byte[] jsonByte, Class<?> cls) {
		try {
			Object bean = objectMapper.readValue(jsonByte, cls);
			return bean;
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_JSON_TO_BEAN_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
	}

	/**
	 * 将json字节数组转换成java对象
	 * 
	 * @param json
	 *            准备转换的json字符串
	 * @param cls
	 *            准备转换的类
	 * @return
	 * @throws Exception
	 */
	public static <T> T jsonToRealBean(byte[] jsonByte, Class<T> cls) {
		try {
			T bean = objectMapper.readValue(jsonByte, cls);
			return bean;
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_JSON_TO_BEAN_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
	}

	/**
	 * 将json字节数组转换成java对象
	 * 
	 * @param json
	 *            准备转换的json字符串
	 * @param cls
	 *            准备转换的类
	 * @return
	 * @throws Exception
	 */
	public static <T> T jsonToRealBean(String json, Class<T> cls) {
		try {
			T bean = objectMapper.readValue(json, cls);
			return bean;
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_JSON_TO_BEAN_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
	}

	/**
	 * 根据json path获取对象
	 * 
	 * @param json
	 *            准备转换的json字符串
	 * @param path
	 *            被获取节点的路径
	 * @return 节点对象
	 */
	public static <T> List<T> getObjectByPath(String json, String tagPath, Class<T> cls) {
		if (EmptyUtils.isEmpty(json) || (EmptyUtils.isEmpty(tagPath))) {
			return null;
		}
		List<T> value = new ArrayList<T>();
		try {
			String[] paths = tagPath.split(":");
			JsonNode jsonNode = objectMapper.readTree(json);
			if (EmptyUtils.isEmpty(jsonNode)) {
				return null;
			}
			getJsonValue(jsonNode, paths, value, 1);
			return value;
		} catch (Exception e) {
			String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_UTIL_JSON_TO_BEAN_ERROR;
			String errorMessage = PropertyConfigurer.getErrorMessage(errorCode);
			logger.error(errorMessage, e);
			throw new FastDemoSystemException(errorCode, errorMessage, e);
		}
	}

	/**
	 * 
	 * @param node
	 * 			json节点
	 * @param path
	 * 			取值路径
	 * @param values
	 * 			返回值
	 * @param nextIndex
	 * 			下一节点
	 */
	@SuppressWarnings({ "unchecked" })
	private static <T> void getJsonValue(JsonNode node, String[] path, List<T> values, int nextIndex) {
		if (EmptyUtils.isEmpty(node)) {
			return;
		}
		if (nextIndex == path.length) {
			if (node.isArray()) {
				for (int i = 0; i < node.size(); i++) {
					JsonNode child = node.get(i).get(path[nextIndex - 1]);
					if (EmptyUtils.isEmpty(child)) {
						continue;
					}
					values.add((T)child);
				}
			} else {
				JsonNode child = node.get(path[nextIndex - 1]);
				if (!EmptyUtils.isEmpty(child)) {
					values.add((T)child);
				}
			}
			return;
		}
		node = node.get(path[nextIndex - 1]);
		if (EmptyUtils.isEmpty(node)) {
			return;
		}
		if (node.isArray()) {
			for (int i = 0; i < node.size(); i++) {
				getJsonValue(node.get(i), path, values, nextIndex + 1);
			}
		} else {
			getJsonValue(node, path, values, nextIndex + 1);
		}
	}
	
}