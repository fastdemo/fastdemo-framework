package com.fastdemo.framework.utils;

import java.util.Date;

public class TradeUtils {

	public static final String ORDER = "11";

	public static final String FLOW = "22";

	/**
	 * 获取订单号
	 * 
	 * @return
	 */
	public static String getOrderNo() {
		String now = DateUtils.formatDatetime(new Date(), "yyyyMMddHHmmss");
		int random = (int) ((Math.random() * 9 + 1) * 100000);
		return now + ORDER + random;
	}

	/**
	 * 获取流水号
	 * 
	 * @return
	 */
	public static String getFlowNo() {
		String now = DateUtils.formatDatetime(new Date(), "yyyyMMddHHmmss");
		int random = (int) ((Math.random() * 9 + 1) * 100000);
		return now + FLOW + random;
	}
}
