package com.fastdemo.framework.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringUtils implements ApplicationContextAware{

	private static ApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		SpringUtils.applicationContext = applicationContext;
	}

	public static ApplicationContext getApplicationContext() {  
        return applicationContext;  
    }
	
	public static Object getBean(String name) throws BeansException {  
        return applicationContext.getBean(name);  
    }
	
	@SuppressWarnings("unchecked")
	public static Object getBeanByType(@SuppressWarnings("rawtypes") Class claz){
		return applicationContext.getBean(claz);
	}
	
}
