package com.fastdemo.framework.utils;

import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;

public class SqlUtils {

	public static final String SQL_COUNT = "COUNT(*)";
	
	/** 注释掉示例代码
	public static String removeOrderBy(String sql) throws JSQLParserException {
		Statement stmt = CCJSqlParserUtil.parse(sql);
		Select select = (Select) stmt;
		SelectBody selectBody = select.getSelectBody();
		processSelectBody(selectBody);
		return select.toString();
	}

	public static void processSelectBody(SelectBody selectBody) {
		if (selectBody instanceof PlainSelect) {
			processPlainSelect((PlainSelect) selectBody);
		} else if (selectBody instanceof WithItem) {
			WithItem withItem = (WithItem) selectBody;
			if (withItem.getSelectBody() != null) {
				processSelectBody(withItem.getSelectBody());
			}
		} else {
			SetOperationList operationList = (SetOperationList) selectBody;
			if (operationList.getPlainSelects() != null
					&& operationList.getPlainSelects().size() > 0) {
				List<PlainSelect> plainSelects = operationList
						.getPlainSelects();
				for (PlainSelect plainSelect : plainSelects) {
					processPlainSelect(plainSelect);
				}
			}
			if (!orderByHashParameters(operationList.getOrderByElements())) {
				operationList.setOrderByElements(null);
			}
		}
	}

	public static void processPlainSelect(PlainSelect plainSelect) {
		if (!orderByHashParameters(plainSelect.getOrderByElements())) {
			plainSelect.setOrderByElements(null);
		}
		if (plainSelect.getFromItem() != null) {
			processFromItem(plainSelect.getFromItem());
		}
		if (plainSelect.getJoins() != null && plainSelect.getJoins().size() > 0) {
			List<Join> joins = plainSelect.getJoins();
			for (Join join : joins) {
				if (join.getRightItem() != null) {
					processFromItem(join.getRightItem());
				}
			}
		}
	}

	public static void processFromItem(FromItem fromItem) {
		if (fromItem instanceof SubJoin) {
			SubJoin subJoin = (SubJoin) fromItem;
			if (subJoin.getJoin() != null) {
				if (subJoin.getJoin().getRightItem() != null) {
					processFromItem(subJoin.getJoin().getRightItem());
				}
			}
			if (subJoin.getLeft() != null) {
				processFromItem(subJoin.getLeft());
			}
		} else if (fromItem instanceof SubSelect) {
			SubSelect subSelect = (SubSelect) fromItem;
			if (subSelect.getSelectBody() != null) {
				processSelectBody(subSelect.getSelectBody());
			}
		} else if (fromItem instanceof ValuesList) {

		} else if (fromItem instanceof LateralSubSelect) {
			LateralSubSelect lateralSubSelect = (LateralSubSelect) fromItem;
			if (lateralSubSelect.getSubSelect() != null) {
				SubSelect subSelect = (SubSelect) (lateralSubSelect
						.getSubSelect());
				if (subSelect.getSelectBody() != null) {
					processSelectBody(subSelect.getSelectBody());
				}
			}
		}
		// Table时不用处理
	}

	public static boolean orderByHashParameters(List<OrderByElement> orderByElements) {
		if (orderByElements == null) {
			return false;
		}
		for (OrderByElement orderByElement : orderByElements) {
			if (orderByElement.toString().toUpperCase().contains("?")) {
				return true;
			}
		}
		return false;
	}**/
	
	public static String buildCountSql(String sql) throws JSQLParserException {
		Statement stmt = CCJSqlParserUtil.parse(sql);
		Select select = (Select) stmt;
		SelectBody selectBody = select.getSelectBody();
		if (selectBody instanceof PlainSelect) {
			//删除order子句
			PlainSelect plainSelect = (PlainSelect) selectBody;
			plainSelect.setOrderByElements(null);
			//构建count列
			Column column = new Column();
			column.setColumnName(SQL_COUNT);
			//组装查询
			SelectExpressionItem selectItem = new SelectExpressionItem();
			selectItem.setExpression(column);
			//插入sql语句中
			List<SelectItem> list = new ArrayList<SelectItem>();
			list.add(selectItem);
			plainSelect.setSelectItems(list);
		}
		return select.toString();
	}

	public static void main(String[] args) throws JSQLParserException {
		// String upperCaseSql = removeBreakingWhitespace(sql.toUpperCase());
		// //替换查询字段为COUNT
		// int fromStartIndex = upperCaseSql.indexOf(SQL_FROM);
		// //剔除ORDER BY字段
		// int orderStartIndex = upperCaseSql.indexOf(SQL_ORDER);
		// System.out.println(upperCaseSql);
		// String tempSql =
		// removeBreakingWhitespace(sql).substring(fromStartIndex,
		// orderStartIndex);
		//
		// //组装新的sql
		// StringBuilder sb = new StringBuilder();
		// sb.append(SQL_SELECT);
		// sb.append(" ");
		// sb.append(SQL_COUNT);
		// sb.append(" ");
		// sb.append(tempSql);
		// System.out.println(sb.toString());
		String sql = "select " + "C.USER_REAL_NAME," + "C.SHARE_DEBIT_COUNT,"
				+ "C.CASE_REMARK_COUNT," + "C.ID,C.CARD_OWNER_NAME,"
				+ "C.ACCOUNT_NO," + "C.CASE_AMT," + "C.NATIONAL_CASE_AMT,"
				+ "C.PAID_AMT," + "C.CUSTOM_NAME," + "C.CASE_NO,"
				+ "C.COLLECTOR_ID," + "C.RECLAIM_DATE,"
				+ "C.ADVANCE_RECLAIM_DATE," + "C.ENTRUST_DATE,"
				+ "C.COMPANY_CODE," + "C.COMPANY_NAME," + "UC.URGE_STATUS,"
				+ "UC.IF_NEW_CLUE," + "UC.CASE_TAG_NAME," + "UC.CASE_TAG_DESC,"
				+ "C.CREATE_TIME AS " + "CREATE_TIME," + "C.CASE_STATUS,"
				+ "C.CURRENCY," + "C.IDENTITY_CARD_NO," + "C.DEBT_AMT "
				+ "from " + "EL_OMS_CASE_INFO C " + "LEFT JOIN "
				+ "EL_OMS_CASEINFO_USER_RELATN " + "UC ON C.ID = "
				+ "UC.CASE_ID AND " + "UC.RELATN_STATUS=1 " + "WHERE 1=1 "
				+ "	AND C.IS_DELETE = ? AND C.CASE_STATUS NOT IN (?,?) ORDER by CREATE_TIME desc";
		System.out.println(buildCountSql(sql));
	}

}
