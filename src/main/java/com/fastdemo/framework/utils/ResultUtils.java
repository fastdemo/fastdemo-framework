package com.fastdemo.framework.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;

import com.fastdemo.framework.config.PropertyConfigurer;
import com.fastdemo.framework.constant.ErrorCodeConstants;
import com.fastdemo.framework.dto.BaseDTO;
import com.fastdemo.framework.dto.SearchDTO;
import com.fastdemo.framework.exception.FaseDemoException;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Controller 返回数据结构定义:<br>
 * {"errorCode":0,"errorMessage":"ok","dataset":object}<br>
 * object为对象类型的json,详见各接口注释
 * 
 */
public class ResultUtils {

	/**
	 * json字段
	 */
	public static final String DATA = "data";

	/**
	 * datalist字段
	 */
	public static final String RECORD = "record";

	/**
	 * count字段
	 */
	public static final String COUNT = "count";

	/**
	 * json code字段
	 */
	public static final String ERROR_CODE = "code";

	/**
	 * json message字段
	 */
	public static final String ERROR_MESSAGE = "message";

	public static final String PAGE = "page";

	/**
	 * HTTP协议controller接口调用成功,正常返回. 样例:{"errorCode":0,"errorMessage":"ok"}
	 * 
	 * @return Map
	 */
	public static Map<String, Object> createResult() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_SUCCESS;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		return resultMap;
	}

	/**
	 * HTTP协议controller接口调用成功,正常返回. 样例:{"errorCode":0,"errorMessage":"ok"}
	 * 
	 * @return Map
	 */
	public static Map<String, Object> createSuccessResult(String errorCode) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put(ERROR_CODE, converErrorCode(ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_SUCCESS));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		return resultMap;
	}

	/**
	 * HTTP协议controller接口调用成功,正常返回. 构建Map返回json.
	 * 
	 * @return Map
	 */
	public static Map<String, Object> createMapResult(Map<String, Object> map) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_SUCCESS;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		resultMap.put(DATA, map);
		return resultMap;
	}

	/**
	 * HTTP协议controller接口调用成功,构建List返回json. 样例:
	 * 
	 * @param baseDTOs
	 * @return
	 */
	public static <T extends BaseDTO> Map<String, Object> createListResult(
			List<T> baseDTOs) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_SUCCESS;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		resultMap.put(DATA, baseDTOs);
		return resultMap;
	}

	/**
	 * HTTP协议controller接口调用成功,构建Page返回json. 样例:
	 * 
	 * @param pageDTO
	 * @return
	 */
	public static <T extends BaseDTO> Map<String, Object> createSearchResult(
			SearchDTO<T> searchDTO) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_SUCCESS;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		resultMap.put(DATA, searchDTO);
		return resultMap;
	}

	/**
	 * HTTP协议controller接口调用成功,构建DTO返回json. 样例:
	 * 
	 * @param pageDTO
	 * @return
	 */
	public static <T extends BaseDTO> Map<String, Object> createDtoResult(
			T baseDTO) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_SUCCESS;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		resultMap.put(DATA, baseDTO);
		return resultMap;
	}

	/**
	 * HTTP协议controller接口调用成功,构建DTO返回json. 样例:
	 * 
	 * @param pageDTO
	 * @return
	 */
	public static Map<String, Object> createCountResult(Integer count) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_SUCCESS;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put(COUNT, count);
		resultMap.put(DATA, dataMap);
		return resultMap;
	}

	/**
	 * HTTP协议controller接口调用成功,构建Dec异常返回json. 样例:
	 * 
	 * @param pageDTO
	 * @return
	 */
	public static Map<String, Object> createPurangExceptionResult(
			FaseDemoException purangException) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put(ERROR_CODE, converErrorCode(purangException.getCode()));
		resultMap.put(ERROR_MESSAGE, purangException.getMessage());
		return resultMap;
	}

	public static Map<String, Object> createBindExceptionResult(
			BindException bindException) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_VALID_EXCEPTION;
		BindingResult result = bindException.getBindingResult();
		List<FieldError> fes = result.getFieldErrors();
		StringBuilder sb = new StringBuilder();
		if (fes != null && fes.size() > 0) {
			boolean flag = false;
			for (FieldError fe : fes) {
				if (flag) {
					sb.append(",");
				}
				sb.append("[");
				sb.append(fe.getCode());
				sb.append("]");
				sb.append(fe.getDefaultMessage());
				flag = true;
			}
		}
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE, sb.toString());
		return resultMap;
	}

	/**
	 * http 请求消息体为空
	 * 
	 * @param httpMessageNotReadableException
	 * @return
	 */
	public static Map<String, Object> createHttpMessageNotReadableExceptionResult(
			HttpMessageNotReadableException httpMessageNotReadableException) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_BODY_ERROR;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE, PropertyConfigurer.getErrorMessage(
				errorCode, httpMessageNotReadableException.getMessage()));
		return resultMap;
	}

	/**
	 * 上传文件异常
	 * 
	 * @param multipartException
	 * @return
	 */
	public static Map<String, Object> createMultipartExceptionResult(
			MultipartException multipartException) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_UPLOAD_FILE_ERROR;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE, PropertyConfigurer.getErrorMessage(
				errorCode, multipartException.getMessage()));
		return resultMap;
	}

	/**
	 * 上传文件超过最大限制
	 * 
	 * @param httpMessageNotReadableException
	 * @return
	 */
	public static Map<String, Object> createMaxUploadSizeExceededExceptionResult(
			MaxUploadSizeExceededException maxUploadSizeExceededException) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_UPLOAD_FILE_EXCEED;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE, PropertyConfigurer.getErrorMessage(
				errorCode, maxUploadSizeExceededException.getMessage()));
		return resultMap;
	}

	public static Map<String, Object> createMethodArgumentNotValidExceptionResult(
			MethodArgumentNotValidException methodArgumentNotValidException) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_VALID_EXCEPTION;
		BindingResult result = methodArgumentNotValidException
				.getBindingResult();
		List<FieldError> fes = result.getFieldErrors();
		String checkMsg = fes.get(0).getDefaultMessage();
		String checkCode = fes.get(0).getCode();
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE, "[" + checkCode + "]:" + checkMsg);
		return resultMap;
	}

	public static Map<String, Object> createJsonProcessingExceptionResult(
			JsonProcessingException jsonProcessingException) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_JSON_EXCEPTION;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE, jsonProcessingException.getMessage());
		resultMap.put(DATA,
				ExceptionUtils.getFullStackTrace(jsonProcessingException));
		return resultMap;
	}

	public static Map<String, Object> createHttpMessageConversionExceptionResult(
			HttpMessageConversionException httpMessageConversionException) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_MESSAGE_CONVERT_EXCEPTION;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				httpMessageConversionException.getMessage());
		resultMap.put(DATA, ExceptionUtils
				.getFullStackTrace(httpMessageConversionException));
		return resultMap;
	}

	public static Map<String, Object> createLoginNeedResult() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_LOGIN_NEED;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		return resultMap;
	}
	
	public static Map<String, Object> createOtherPlaceLoginErrorResult() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_OTHER_PLACE_LOGIN_ERROR;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		return resultMap;
	}

	public static Map<String, Object> createSecurityNeedResult() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_SECURITY_NOT_DFEFINE;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		return resultMap;
	}

	public static Map<String, Object> createPermissionNotAllowExceptionResult() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_PERMISSION_ERROR;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		return resultMap;
	}

	public static Map<String, Object> createHTTPResourceNotAllowExceptionResult() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_RESOURCE_NOT_ALLOW;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		return resultMap;
	}

	public static Map<String, Object> createSimplErrorResult(String errorCode) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		return resultMap;
	}
	
	public static Map<String, Object> createSimplErrorResult(String errorCode,Object...params) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode,params));
		return resultMap;
	}

	public static Map<String, Object> createSimplErrorResult(String errorCode,
			String message) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE, message);
		return resultMap;
	}

	/**
	 * HTTP协议controller接口调用成功,构建Unknow Exception返回json. 样例:
	 * 
	 * @param pageDTO
	 * @return
	 */
	public static Map<String, Object> createUnknowExceptionResult(
			Throwable throwable) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_EXCEPTION;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		resultMap.put(DATA, ExceptionUtils.getFullStackTrace(throwable));
		return resultMap;
	}

	/**
	 * 非HandlerMethod 请求
	 * 
	 * @param pageDTO
	 * @return
	 */
	public static Map<String, Object> createHTTPHandleExceptionResult() {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_VALID_EXCEPTION;
		resultMap.put(ERROR_CODE,
				Integer.valueOf(errorCode.substring(1, errorCode.length())));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		return resultMap;
	}

	public static Integer converErrorCode(String errorCode) {
		return Integer.valueOf(errorCode.substring(1, errorCode.length()));
	}
	
	/**
	 * HTTP协议controller接口调用成功,构建Page返回json. 样例:
	 * 
	 * @param pageDTO
	 * @return
	 */
	public static <T extends BaseDTO> Map<String, Object> createDataTableSearchResult(
			SearchDTO<T> searchDTO) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String errorCode = ErrorCodeConstants.MSG_FASTDEMO_FRAMEWORK_HTTP_SUCCESS;
		resultMap.put(ERROR_CODE, converErrorCode(errorCode));
		resultMap.put(ERROR_MESSAGE,
				PropertyConfigurer.getErrorMessage(errorCode));
		if(searchDTO != null) {
			Integer totalCount = searchDTO.getPageDTO().getTotalCount();
			List<T> records = searchDTO.getRecords();
			resultMap.put(DATA, records);
			resultMap.put("iTotalDisplayRecords", totalCount);
			resultMap.put("iTotalRecords", totalCount);
		} else {
			resultMap.put(DATA, null);
		}
		return resultMap;
	}

}
